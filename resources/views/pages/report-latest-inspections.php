@extends('layout.master')
@section('contents')
<table class="table" id="report_main_table">
        <thead class="table-dark">
            <tr>
                <th>#</th>
				<th>کد شعبه</th>
                <th>نام شعبه</th>
                <th>کل سوالات</th>
                <th>پاسخ داده شده</th>
				<th>وضعیت بازرسی</th>
				<th>نوع شعبه</th>
				<th>صندوق اجاره ای</th>
				<th>شروع بازرسی</th>
				<th>پایان بازرسی</th>
				<th>عملیات</th>
            </tr>
        </thead>
        <tbody>
			@php
			$i=1;
			@endphp
			@foreach($reportData as $brcode=>$inspItem)
			
			@php
			$checklistDetailsUrl = route('inspection.checklist.show-final-page',['brcode'=>$inspItem['branch_code'], 'period'=>$inspItem['period'], 'inspection_date'=> str_replace('/','-',$inspItem['started_at_jalali'])]);
			@endphp
			
            <tr data-sarcode="{{$brcode}}">
				<td data-href="#content{{$brcode}}">{{$i++}}</td>
				<td data-href="#content{{$brcode}}"><a href="{{$checklistDetailsUrl}}" target="_blank">{{$inspItem['branch_code']}}</a></td>
                <td data-href="#content{{$brcode}}"><a href="{{$checklistDetailsUrl}}" target="_blank">{{$inspItem['branch_name']}}</a></td>
                <td data-href="#content{{$brcode}}">{{$inspItem['total_questions_scount']}}</td>
				<td data-href="#content{{$brcode}}">{{$inspItem['respond_questions_count']}}</td>
                <td data-href="#content{{$brcode}}">{{$inspItem['branch_checklist_status_title']}}</td>
                <td data-href="#content{{$brcode}}">{{$inspItem['is_arzi']}}</td>
				<td data-href="#content{{$brcode}}">{{$inspItem['has_rental_box']}}</td>
				<td data-href="#content{{$brcode}}">{{$inspItem['started_at_jalali']}}</td>
				<td data-href="#content{{$brcode}}">{{$inspItem['ended_at_jalali']}}</td>
				<td><a class="btn btn-success" href="{{route('inspection.checklist.showQuestionPage',['brcode'=>$inspItem['branch_code'], 'inspection_date'=> str_replace('/','-',$inspItem['started_at_jalali']), 'category_code'=>'CUSTOMER_IDENTIFICATION', 'rental_box'=>$inspItem['has_rental_box_value'] ] )}}">ثبت پاسخ رئیس شعبه</a></td>
            </tr>
			@endforeach
        </tbody>
    </table>


@endsection
@section('bottom-js-scripts')

@endsection

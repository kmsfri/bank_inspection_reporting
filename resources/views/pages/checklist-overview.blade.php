@extends('layout.master')
@section('top-libraries')
<link rel="stylesheet" href="{{asset('libs/datepicker/css/persianDatepicker-default.css')}}" />
@endsection
@section('contents')

<div class="row" id="categories-container">
  <div class="col-12">
    <ul class="nav nav-justified dir-ltr">
      @foreach($allCategories as $categoryItem)
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">{{$categoryItem->TITLE}}</a>
      </li>
      @endforeach
    </ul>
  </div>
</div>

@if($completedChecklistMessage)
<div class="row text-center" id="checklist-container">
  <div class="col-12 mt-5">
	<div class="alert alert-success">
		{{$completedChecklistMessage}}
	</div>
  </div>
</div>
@endif

@if (\Session::has('success'))
<div class="alert alert-success">
	<ul>
		<li>{!! \Session::get('success') !!}</li>
	</ul>
</div>
@endif

<form id="checklist-overview-form" action="{{route('inspection.checklist.finalize')}}" method="POST">

<input type="hidden" name="store_step" value="{{$storeStep}}">

<div class="row text-center" id="checklist-container">
  <div id="checklist-overview-container" class="col-8 mt-5">
    <div class="row">
      <div class="col-12 mb-2">
        <h5 class="overview-headline">بررسی نهایی</h5>
      </div>
    </div>
    <div class="row">
      <div class="col-3">
        <label class="overview-label">نام شعبه</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$selectedBrName}}</span>
      </div>
    </div>
    <div class="row">
      <div class="col-3">
        <label class="overview-label">کد شعبه</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$selectedBrCode}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">نام حوزه تحت پوشش</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$branchclassification->bazhozename}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">نام امور شعب</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$branchclassification->sarname}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">درجه شعبه</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$darajehTitle}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">امتیاز</label>
      </div>
      <div class="col-9">
		<span class="overview-value {{$scoreStatusArray['style_class']}}">{{$scoreStatusArray['avg']}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">وضعیت بازرسی</label>
      </div>
      <div class="col-9">
		<span class="overview-value {{$scoreStatusArray['style_class']}}">{{$scoreStatusArray['title']}}</span>
      </div>
    </div>
	
    <div class="row">
      <div class="col-3">
        <label class="overview-label">تاریخ شروع بازرسی</label>
      </div>
      <div class="col-9">
        <span class="overview-value">{{$inspectionStartDate}}</span>
      </div>
    </div>
	
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">امتیاز دوره قبل</label>
      </div>
      <div class="col-9">
		<span class="overview-value {{$previousPeriodScoreStatusArray['style_class']}}">{{$previousPeriodScoreStatusArray['avg']}}</span>
      </div>
    </div>
	
	<div class="row">
      <div class="col-3">
        <label class="overview-label">وضعیت بازرسی(دوره قبل)</label>
      </div>
      <div class="col-9">
		<span class="overview-value {{$previousPeriodScoreStatusArray['style_class']}}">{{$previousPeriodScoreStatusArray['title']}}</span>
      </div>
    </div>

    <div id="checklist-overview-info-container">
    <div class="row info-row1">
      <div class="col-6">
        <div class="row">
          <div class="col-4">
            <label class="overview-label">نام بازرس</label>
          </div>
          <div class="col-8">
            {!! implode('</br>',$inspectors) !!}
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="row">
          <div class="col-4">
            <label class="overview-label">نام رئیس شعبه</label>
          </div>
          <div class="col-8">
            <span id="branch_boss_name_container">{{$branchBoss}}</span>
			<input {{(($user_type!="SARPARASTI_INSPECTOR" && $user_type!="GENERAL_INSPECTOR") || $disabledFields)?"disabled":""}} type="text" name="branch_boss_personnel_code" id="branch_boss_personnel_code" class="form-control hide" value="" placeholder="کد پرسنلی رئیس شعبه">
			@if(!(($user_type!="SARPARASTI_INSPECTOR" && $user_type!="GENERAL_INSPECTOR") || $disabledFields))
			&nbsp;(<a href="javascript:void()" onclick="showBranchBossField()">تغییر</a>)
			@endif
          </div>
        </div>
      </div>
    </div>

    <div class="row info-row2">
      <div class="col-6">
        <div class="row">
          <div class="col-4">
            <label class="overview-label">تاریخ بازرسی قبلی</label>
          </div>
          <div class="col-8">
            بازرسی نشده
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="row">
          <div class="col-4">
            <label class="overview-label">بازرس دوره قبلی</label>
          </div>
          <div class="col-8">
            بازرسی نشده
          </div>
        </div>
      </div>
    </div>


	
    </div>



	<input type="hidden" name="brcode" value="{{$selectedBrCode}}">
	<input type="hidden" name="period" value="{{$period}}">
	<input type="hidden" name="inspection_date" value="{{$inspectionDate}}">

	<div class="row">
	  <div class="col-12 text-end dir-rtl">
		<label for="inspector_description">توضیحات بازرس:</label>
		<textarea {{(($user_type!="SARPARASTI_INSPECTOR" && $user_type!="GENERAL_INSPECTOR") || $disabledFields)?"disabled":""}} autocomplete="off" class="form-control" rows="4" name="inspector_description">{{$inspectorResponse??''}}</textarea>
	  </div>
	  
	</div> 
	
	<div class="row">
	  <div class="col-6 text-end dir-rtl">
		<label for="inspector_program_number">شماره حکم بازرسی:</label>
		<input type="number" value="{{$inspectorProgramNumber??''}}" {{(($user_type!="SARPARASTI_INSPECTOR" && $user_type!="GENERAL_INSPECTOR") || $disabledFields)?"disabled":""}} autocomplete="off" class="form-control" name="inspector_program_number">
	  </div>
	  
	  <div class="col-6 text-end dir-rtl">
		<label for="inspector_program_date">تاریخ حکم بازرسی:</label>
		<input type="text" value="{{$inspectorProgramDate??''}}" {{(($user_type!="SARPARASTI_INSPECTOR" && $user_type!="GENERAL_INSPECTOR") || $disabledFields)?"disabled":""}} autocomplete="off" class="form-control j-date-calendar" name="inspector_program_date" id="inspector_program_date">
	  </div>
	  
	</div> 

	<div class="row mt-5 overview-last-row-cont">
	  <div class="col-6 text-start">
		@if($respondAllRequiredQuestions)
			@if($storeStep==1)
			<button type="submit" class="btn btn-success">ثبت موقت</button>
			@elseif($storeStep==2)
			<button type="submit" class="btn btn-success">ثبت نهایی</button>
			@else
			<button type="button" disabled class="btn btn-success">ثبت</button>
			@endif
		@else
			<button type="button" disabled class="btn btn-success">ثبت نهایی</button>
		@endif
	  </div>
	  <div class="col-6 text-end">
		<a href="{{route('inspection.checklist.showQuestionPage',['brcode'=>$selectedBrCode,'inspection_date'=>$inspectionDate,'category_code'=>'CUSTOMER_IDENTIFICATION','rental_box'=>$rentalBox])}}" class="btn btn-secondary">بازبینی مجدد</a>
	  </div>
	</div>


  </div>
</div>
</form>


@endsection

@section('bottom-js-scripts')

<script src="{{asset('libs/datepicker/persian-date.js')}}"></script>
<script src="{{asset('libs/datepicker/js/persianDatepicker.min.js')}}"></script>

<script>
$(document).ready(function() {
  
  
	var p = new persianDate();
    var today = p.now().toString("YYYY/MM/DD");
  
    $("#inspector_program_date").persianDatepicker({
        formatDate: "YYYY/MM/DD",
        selectableYears: [1403,1404],
		selectedDate: 'today',
		startDate: '1403/01/01',
		endDate: "today"
    });
	
	@if(empty($inspectorProgramDate))
		$("#inspector_program_date").val(today);
	@endif
	
	
});

function showBranchBossField(){
	if($('#branch_boss_name_container').hasClass('hide')){
		$('#branch_boss_name_container').removeClass('hide');
		$('#branch_boss_personnel_code').val('');
		$('#branch_boss_personnel_code').addClass('hide');
	}else{
		$('#branch_boss_name_container').addClass('hide');
		$('#branch_boss_personnel_code').removeClass('hide');
	}
	
}
</script>

@endsection

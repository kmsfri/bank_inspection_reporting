@extends('layout.master')
@section('contents')

<table class="table" id="report_main_table">
        <thead class="table-dark">
            <tr>
                <th>#</th>
				<th>کد اداره امور</th>
                <th>اداره امور</th>
                <th>تعداد کل شعب</th>
                <th>شعب تکمیل شده</th>
				<th>شعب در حال تکمیل</th>
            </tr>
        </thead>
        <tbody>
			@php
			$i=1;
			@endphp
			@foreach($reportData as $sarparastiCode=>$reportItem)
            <tr data-sarcode="{{$sarparastiCode}}">
				<td data-href="#content{{$sarparastiCode}}">{{$i++}}</td>
				<td data-href="#content{{$sarparastiCode}}">{{$reportItem['sarparasti_code']}}</td>
                <td data-href="#content{{$sarparastiCode}}">{{$reportItem['sarparasti_name']}}</td>
                <td data-href="#content{{$sarparastiCode}}">{{$reportItem['branches_count']}}</td>
                <td data-href="#content{{$sarparastiCode}}">{{$reportItem['complete_branches_count']}}</td>
                <td data-href="#content{{$sarparastiCode}}">{{$reportItem['in_completing_branches_count']}}</td>
            </tr>
			@endforeach
        </tbody>
    </table>
	@foreach($reportData as $sarparastiCode=>$reportItem)
	<div id="content{{$sarparastiCode}}">
		123456t
	</div>
	@endforeach


@endsection

@section('bottom-js-scripts')

<script>
$(document).ready(function() {

	$("td").click(function() {
	 var thisRow = $(this).closest('tr'); //parent row of the input or whatever is the click trigger
	 var conDiv = $(this).data("href"); //returns #content1 - id of the content div you want shown
	 var conH = $(conDiv).height(); //corresponding content div height
	 var rowH = $(thisRow).height(); // this row height
	 var newrowH = conH + rowH; //the new row height
	 //var posL = $(thisRow).position().left; // left position that div needs to be
	 var posT = $(thisRow).offset().top + rowH  // top position that div needs to be ** not working properly!!
	 

	   if ( $(conDiv).is(":visible") ) {
		$(thisRow).css({"height" : "auto"});
		$(conDiv).css({"display" : "none", "left": "0", "top" : "auto" });
	   } else  {
		
		//reset all rows to normal height
		$("tr").css({"height" : "auto"}); 
		//expand the height of this row
		
	   
		// reset all content divs.. hide them             
		$("[id*=content]").css({"display" : "none", "left": "0", "top" : "auto"}); 
		//show the one you want
		//$(conDiv).css({"display" : "block", "left": posL, "top" : posT});
		$(conDiv).css({"display" : "block", "top" : posT});
		
		var selectedSarcode = thisRow.data('sarcode');
		
		//get the branches statistic(ajax)
		$.ajax({
		
			url: '{{route('inspection.report.sarparasti.get-sarparasti-branches-report')}}',
			type: 'POST',
			cache:false,
			data: {sarcode:selectedSarcode},
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			success: function(response){

				
				$(conDiv).html(response);
				
				
				
				$(thisRow).css({"height" : $(conDiv).height()});
				
			}
		});
		
		
		
	   }
	});

});


</script>


@endsection


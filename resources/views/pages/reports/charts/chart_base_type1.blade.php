@extends('layout.master')

@section('top-libraries')

<script src="//code.highcharts.com/highcharts.js"></script>
<script src="//code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<link rel="stylesheet" href="{{asset('libs/datepicker/css/persianDatepicker-default.css')}}" />
<link rel="stylesheet" href="{{asset('libs/datatable/dataTables.bootstrap5.css')}}" />
<link rel="stylesheet" href="{{asset('libs/datatable/fixedColumns.bootstrap5.css')}}" />
@endsection
@section('contents')


<form action="{{route('inspection.report.show-time-based-chart',['based_data'=>$basedData, 'criteria'=> $criteria])}}" class="mb-5">
  <div class="row">
	<div class="col-4">
	  <input type="text" class="form-control" placeholder="از تاریخ" name="from_date" id="from_date" value="{{request()->from_date}}">
	</div>
	<div class="col-4">
	  <input type="text" class="form-control" placeholder="تا تاریخ" name="to_date" id="to_date" value="{{request()->to_date}}">
	</div>
  </div>
  <div class="row">
	<div class="col text-start">
		<button type="submit" class="btn btn-success">فیلتر</button>
	</div>
  </div>
</form>

<div id="chart1"></div>

<div class="row">
	<div class="col-12">
		<table id="data_list_table" class="table table-striped nowrap mt-3" style="width:100%">

			<thead>
				<tr>
					<th scope="col">ردیف</th>
					<th scope="col">اداره امور</th>
					<th scope="col">کد اداره امور</th>
					<th scope="col">امتیاز کل</th>
					<th scope="col">آخرین بازرسی</th>
					<th scope="col">آخرین بروزرسانی</th>
				</tr>
			</thead>
			<tbody>
			
			@foreach($data_list as $key=>$dataRow)
			<tr>
				<th scope="row">{{$key}}</th>
				<td>{{$dataRow['sarname']}}</td>
				<td>{{$dataRow['sarcode']}}</td>
				<td>{{$dataRow['total_score']}}</td>
				<td>{{$dataRow['max_process_month']}}</td>
				<td>{{$dataRow['max_updated_at']}}</td>
			</tr>
			@endforeach

			</tbody>
			
		</table>
		
	</div>
</div>

{!! $chart1 !!}

@endsection

@section('bottom-js-scripts')
<script src="{{asset('libs/datepicker/persian-date.js')}}"></script>
<script src="{{asset('libs/datepicker/js/persianDatepicker.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
  
  
	var p = new persianDate();
    var today = p.now().toString("YYYY-MM-DD");
  
    $("#from_date").persianDatepicker({
        formatDate: "YYYY-MM-DD",
        selectableYears: [1402,1403],
		selectedDate: '{{!empty(request()->from_date)?str_replace("-","/",request()->from_date):""}}',
		startDate: '1403/01/01',
		endDate: "today"
    });
	
	$("#to_date").persianDatepicker({
        formatDate: "YYYY-MM-DD",
        selectableYears: [1402,1403],
		selectedDate: '{{!empty(request()->to_date)?str_replace("-","/",request()->to_date):""}}',
		startDate: '1403/01/01',
		endDate: "today"
    });
	
	new DataTable('#data_list_table', {
		fixedColumns: true,
		paging: false,
		scrollCollapse: true,
		scrollX: true,
		scrollY: 300
	});
	
  });
  
  
  
  

  


  
  
  
</script>
@endsection


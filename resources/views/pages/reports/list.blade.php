@extends('layout.master')

@section('bottom-js-scripts')
@endsection

@section('contents')

@if(auth()->user()->user_type!="BRANCH_BOSS")
<div class="col-4 text-center mt-3 reports-list-item-cont">
	<a href="{{route('inspection.report.show-report-page')}}" class="btn btn-success">
		گزارش ثبت چک لیست
	</a>
</div>
@endif


<div class="col-4 text-center mt-3 reports-list-item-cont">
	<a href="{{route('inspection.report.show-latest-inspections')}}" class="btn btn-success">
		آخرین ثبت شده ها
	</a>
</div>

<div class="col-4 text-center mt-3 reports-list-item-cont">
	<a href="{{route('inspection.report.show-time-based-chart',['based_data'=>'sarparasties','criteria'=>'score'])}}" class="btn btn-success">
		نمودار ادارات امور
	</a>
</div>

<div class="col-4 text-center mt-3 reports-list-item-cont">
	<a href="{{route('inspection.report.show-time-based-chart',['based_data'=>'bank','criteria'=>'score'])}}" class="btn btn-success">
		نمودار کل بانک
	</a>
</div>

@endsection


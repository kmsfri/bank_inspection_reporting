<table class="table">
	<thead class="table-primary">
		<tr>
		<th>#</th>
		<th>کد شعبه</th>
		<th>نام شعبه</th>
		<th>کل سوالات</th>
		<th>پاسخ داده شده</th>
		<th>وضعیت بازرسی</th>
		<th>وضعیت شعبه</th>
		<th>صندوق اجاره ای</th>
	</tr>
	</thead>
		<tbody>
		@php
			$i=1;
		@endphp
		@foreach($reportData as $branchCode=>$reportItem)
			<tr data-brcode={{$branchCode}}>
				<td>{{$i++}}</td>
				<td>{{$reportItem['branch_code']}}</td>
				<td><a href="{{route('inspection.report.show-inspection-records',$branchCode)}}" target="_blank">{{$reportItem['branch_name']}}</a></td>
				<td>{{$reportItem['total_questions_scount']}}</td>
				<td>{{$reportItem['respond_questions_count']}}</td>
				<td>{{$reportItem['branch_checklist_status_title']}}</td>
				<td>{{$reportItem['is_arzi']}}</td>
				<td>{{$reportItem['has_rental_box']}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

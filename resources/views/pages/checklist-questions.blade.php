@extends('layout.master')
@section('top-libraries')
<script>
var uploadFileFunction = function(i,obj) {


	var file_data = $(this).prop('files')[0];
	
	var form_data = new FormData();                  
	form_data.append('file', file_data);
	
	var containerInputObject = $(this).parent().find('.uploaded-file-container-input');
	
	$.ajax({
		url: '{{route('inspection.checklist.uploadDocFile')}}',
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(response){
		
		
			var responseArray = $.parseJSON(response);
			if(responseArray.status==200){
				containerInputObject.val(responseArray.docFile);
				
			}else{
				alert('آپلود فایل ما مشکل مواجه شد!');
			}
			
		}
	 });
}

</script>

<link rel="stylesheet" href="{{asset('libs/datepicker/css/persianDatepicker-default.css')}}" />
@endsection
@section('contents')

<div class="row" id="categories-container">
  <div class="col-12">
    <ul class="nav nav-justified dir-ltr">
      @foreach($allCategories as $categoryItem)
      <li class="nav-item">
        <a class="nav-link {{($categoryItem->CODE==$activeCategoryCode)?'active':''}}" aria-current="page" href="{{route('inspection.checklist.showQuestionPage',['brcode'=>$selectedBrCode,'inspection_date'=>$inspectionDate,'category_code'=>$categoryItem->CODE,'rental_box'=>$rentalBox])}}">{{$categoryItem->TITLE}}</a>
      </li>
      @endforeach
    </ul>
  </div>
</div>

<form id="checklist-questions-form">
<div class="row" id="checklist-container">
@foreach($questionGroupUnderTheSelectedCategory as $questionGroupItem)
<div class="col-12">
	<div class="card bg-success text-white category-group-title-container">
		<div class="card-body"><span class="badge bg-success">{{$questionGroupItem->group_no_title}}-</span>{{$questionGroupItem->group_title}}</div>
	</div>
</div>
@foreach($categoryQuestions as $questionItem)
  @php
  if($questionItem->FK_QUESTION_GROUP != $questionGroupItem->id){
	continue;
  }
  @endphp
  <div class="col-12">
    <div class="checklistQuestionContainer">
      <div class="row question-row">
        <div class="col-12">
			<span class="badge bg-secondary">
			{{$questionItem->QUESTION_NO_TITLE}}
			</span>
			{!! nl2br($questionItem->QUESTION) !!}
			&nbsp;&nbsp;
			<span class="badge bg-primary float-start">
				#{{$questionItem->questionTypeCategory()->first()->TITLE}}
			</span>
          @if(!empty($questionItem->subQuestions()->get()) && count($questionItem->subQuestions()->get())>0 )
          <span>
            <i class="arrow down float-start" onclick="openSubQuestions(this,{{$questionItem->ID}})"></i>
          </span>
          @endif
        </div>
        <div class="col-12 text-start dir-ltr all-buttons-container  {{($activeCategoryCode!='GENERAL_CHECK')?'hide':''}}">
          @php
          $textInputs = [];
		  $groupStyles = [];
		  $questionHasButtonResponse = false;
		  
		  $questionAnsweredChoice = "";
		  
		  $counter = 0;
          @endphp
          @foreach($questionChoices[$questionItem->ID] as $choiceItemsGroup)
            @foreach($choiceItemsGroup as $choiceGroupID=>$choiceItems)
              @foreach($choiceItems as $choiceItem)
                @if($choiceItem->FK_LKPQUE_ANS_TYP==1101)
                <div class="question-buttons-container">
				  @php
					$checkedMainChoice = $choicesData[$questionItem->ID][$choiceItem->ID]??false;
					if($checkedMainChoice){
						$questionAnsweredChoice = $checkedMainChoice;
					}
					
					$checkedSubChoice = false;
					
					foreach($choiceItem->subAnswers()->get() as $nestedChoiceItem){
						if(isset($choicesData[$questionItem->ID][$nestedChoiceItem->ID])){
							$checkedSubChoice = $choicesData[$questionItem->ID][$nestedChoiceItem->ID];
							if($checkedSubChoice){
								$questionAnsweredChoice = $checkedSubChoice;
							}
						}
					}
					
				  @endphp
                  <button class="checklist-choice-btn {{$checkedMainChoice?$choiceItem->SELECTED_COLOR:''}}" data-buttonType="{{$buttonTypes[$choiceItem->ID]??""}}" type="button" data-selectedColor="{{$choiceItem->SELECTED_COLOR}}" data-choiceid="{{$choiceItem->ID}}" onclick="openSubChoiceContainer({{$questionItem->ID}},{{$choiceItem->ID}}); selectChoice(this,{{$choiceItem->ID}},{{count($choiceItem->subAnswers()->get())}},{{$questionItem->ID}});">{{$choiceItem->ANSWER_ITEM}}</button>
                  @if(!empty($choiceItem->subAnswers()->get()) && count($choiceItem->subAnswers()->get())>0)
                  <span id="sub-choices-container-{{$questionItem->ID}}-{{$choiceItem->ID}}" class="sub-choice-container {{($checkedMainChoice || $checkedSubChoice)?'':'hide'}}">
                    <!-- nested choice buttons -->
                    @foreach($choiceItem->subAnswers()->get() as $nestedChoiceItem)
                      @if($nestedChoiceItem->FK_LKPQUE_ANS_TYP==1101)
                        <button data-buttonType="{{$buttonTypes[$nestedChoiceItem->ID]??""}}" class="checklist-choice-btn {{($checkedSubChoice && $checkedSubChoice==$nestedChoiceItem->ID)?$choiceItem->SELECTED_COLOR:''}}" type="button" data-selectedColor="{{$nestedChoiceItem->SELECTED_COLOR}}" onclick="selectChoice(this,{{$nestedChoiceItem->ID}},0,{{$questionItem->ID}})">{{$nestedChoiceItem->ANSWER_ITEM}}</button>
                        @if(!empty($nestedChoiceItem->subAnswers()->get()) && count($nestedChoiceItem->subAnswers()->get())>0)
                        <span>

                        </span>
                        @endif
                      @elseif( in_array($nestedChoiceItem->FK_LKPQUE_ANS_TYP ,[1102,1104,1105]) )
                        ---
                      @endif
                    @endforeach
                  </span>
                  @endif
                </div>
				@php
				$questionHasButtonResponse = true;
				@endphp
				
                @elseif( in_array($choiceItem->FK_LKPQUE_ANS_TYP ,[1102,1104,1105]) )
                  @php
				  
				  if($choiceGroupID!=4 || ($choiceGroupID==4&&$isAllCategoryQuestionsAnswered) ){
					$textInputs[$choiceGroupID][] = $choiceItem;
					$groupStyles[$choiceGroupID] = "";
					if($choiceGroupID==1){
					  $groupStyles[$choiceGroupID] = "dir-ltr";
					}
				  }
                  
                  @endphp
                @endif
              @endforeach
            @endforeach
          @endforeach
        </div>
		@if($questionHasButtonResponse)
		<input type="hidden" autocomplete="off" data-questionid="{{$questionItem->ID}}" class="question_selected_choice" id="question_selected_choice_{{$questionItem->ID}}" name="question_selected_choice[{{$questionItem->ID}}]" value="{{$questionAnsweredChoice}}">
		@endif
		
        <div class="col-12">
        <div class="row text-inputs-container question_cont_{{$questionItem->ID}}">
		@foreach($textInputs as $textInputKey=>$choiceGroup)
		<div class="col-12">
		<div class="row question-group-container {{$groupStyles[$textInputKey]}}">
        @foreach($choiceGroup as $choiceItem)
		  @php
		  $explodedResponderUserTypes = explode(',',$choiceItem->RESPONDER_USER_TYPES??NULL);
		  $explodedViewerUserTypes = explode(',',$choiceItem->VIEWER_USER_TYPES??NULL);
		  @endphp
		  @if(!$disabledFields && (empty($choiceItem->RESPONDER_USER_TYPES) || (!empty($choiceItem->RESPONDER_USER_TYPES) && in_array(auth()->user()->user_type, $explodedResponderUserTypes))) )
			  @if($choiceItem->FK_LKPQUE_ANS_TYP==1104)

			  
			  <!--
				documentation responses
			  -->
			  <div  class="col-12">
				<x-choice-documents :questionItem="$questionItem" :choiceItem="$choiceItem" :countFrom="count($documentsData[$questionItem->ID][$choiceItem->ID]??[NULL])" :documentsData="$documentsData[$questionItem->ID][$choiceItem->ID]??[NULL]" :disabled="false" :disabledBrBossFields="$disabledBrBossFields"  />
			  </div>

			  @elseif($choiceItem->FK_LKPQUE_ANS_TYP==1105)
			  @php
			  $maxValue="1000000";
			  if($choiceItem->ID==21){
				$maxValue="100";
			  }
			  @endphp
			  <div class="col-2 text-end dir-rtl">
				<label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}">{{$choiceItem->ANSWER_ITEM}}:</label>
				<input onkeyup=enforceMinMax(this) inputmode="numeric" oninput="this.value = this.value.replace(/\D+/g, '')" type="number" min="0" max="{{$maxValue}}" onfocusout="controlInputNumbers(this,{{$questionItem->ID}}, {{$choiceItem->ID}}); documentCountsChanged(this,{{$questionItem->ID}}, {{$choiceItem->ID}});" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}" class="form-control form-control-sm" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}]" value="{{$textsData[$questionItem->ID][$choiceItem->ID]??""}}"><span></span>
			  </div>
			  
			  @else
			  
			  <div class="col-12 text-end dir-rtl">
				<label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}">{{$choiceItem->ANSWER_ITEM}}:</label>
				<textarea autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}" class="form-control" rows="5" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}]">{{$textsData[$questionItem->ID][$choiceItem->ID]??""}}</textarea>
			  </div>
				
			  @endif
		  @elseif($disabledFields || empty($choiceItem->VIEWER_USER_TYPES) || (!empty($choiceItem->VIEWER_USER_TYPES) && in_array(auth()->user()->user_type, $explodedViewerUserTypes)) )
			@if($choiceItem->FK_LKPQUE_ANS_TYP==1104)
			
			
			  <!--
				documentation responses
			  -->
			  <div  class="col-12">
				<x-choice-documents :questionItem="$questionItem" :choiceItem="$choiceItem" :countFrom="count($documentsData[$questionItem->ID][$choiceItem->ID]??[NULL])" :documentsData="$documentsData[$questionItem->ID][$choiceItem->ID]??[NULL]" :disabled="true" :disabledBrBossFields="$disabledBrBossFields" />
			  </div>
			@elseif($choiceItem->FK_LKPQUE_ANS_TYP==1105)
			  
			  <div class="col-2 text-end dir-rtl">
				<label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}">{{$choiceItem->ANSWER_ITEM}}:</label>
				<input disabled type="number" min="0" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}" class="form-control" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}]" value="{{$textsData[$questionItem->ID][$choiceItem->ID]??""}}"><span></span>
			  </div>
			  
			@else
			  
			  <div class="col-12 text-end dir-rtl">
				<label>{{$choiceItem->ANSWER_ITEM}}:</label>
				<textarea autocomplete="off" disabled data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}]" class="form-control" rows="5">{{$textsData[$questionItem->ID][$choiceItem->ID]??""}}</textarea>
			  </div>
				
			@endif
		  @endif
        @endforeach
		</div>
        </div>
		@endforeach
        </div>
        </div>




      </div>


      @if(!empty($questionItem->subQuestions()->get()) && count($questionItem->subQuestions()->get())>0 )

      <div id="sub-question-container-{{$questionItem->ID}}" class="sub-question-container hide">
      @foreach($questionItem->subQuestions()->get() as $questionSubItem)
      <div class="row question-row">
        <div class="col-12">
		
			<span class="badge bg-secondary">
			{{$questionSubItem->QUESTION_NO_TITLE}}
			</span>
			{!! nl2br($questionSubItem->QUESTION) !!}
			&nbsp;&nbsp;
			<span class="badge bg-primary float-start">
				#{{$questionSubItem->questionTypeCategory()->first()->TITLE}}
			</span>
		
        </div>
        <div class="col-12 text-start dir-ltr all-buttons-container">
          @php
          $textInputs = [];
		  $questionHasButtonResponse = false;
		  
		  $questionAnsweredChoice = "";
          @endphp
          @foreach($questionChoices[$questionSubItem->ID] as $choiceItemsGroup)
            @foreach($choiceItemsGroup as $choiceItems)
              @foreach($choiceItems as $choiceItem)
                @if($choiceItem->FK_LKPQUE_ANS_TYP==1101)
                <div class="question-buttons-container">
				
				  @php
					$checkedMainChoice = $choicesData[$questionSubItem->ID][$choiceItem->ID]??false;
					if($checkedMainChoice){
						$questionAnsweredChoice = $checkedMainChoice;
					}
					
					$checkedSubChoice = false;
					foreach($choiceItem->subAnswers()->get() as $nestedChoiceItem){
						if(isset($choicesData[$questionSubItem->ID][$nestedChoiceItem->ID])){
							$checkedSubChoice = $choicesData[$questionSubItem->ID][$nestedChoiceItem->ID];
							if($checkedSubChoice){
								$questionAnsweredChoice = $checkedSubChoice;
							}
						}
					}
				  @endphp
				
                  <button class="checklist-choice-btn {{$checkedMainChoice?$choiceItem->SELECTED_COLOR:''}}" type="button" data-selectedColor="{{$choiceItem->SELECTED_COLOR}}" onclick="openSubChoiceContainer({{$questionSubItem->ID}},{{$choiceItem->ID}}); selectChoice(this,{{$choiceItem->ID}},{{count($choiceItem->subAnswers()->get())}},{{$questionSubItem->ID}})">{{$choiceItem->ANSWER_ITEM}}</button>
                  @if(!empty($choiceItem->subAnswers()->get()) && count($choiceItem->subAnswers()->get())>0)
                  <span id="sub-choices-container-{{$questionSubItem->ID}}-{{$choiceItem->ID}}" class="sub-choice-container {{($checkedMainChoice || $checkedSubChoice)?'':'hide'}}">
                    <!-- nested choice buttons -->
                    @foreach($choiceItem->subAnswers()->get() as $nestedChoiceItem)
                      @if($nestedChoiceItem->FK_LKPQUE_ANS_TYP==1101)
                        <button class="checklist-choice-btn {{($checkedSubChoice && $checkedSubChoice==$nestedChoiceItem->ID)?$choiceItem->SELECTED_COLOR:''}}" type="button" data-selectedColor="{{$nestedChoiceItem->SELECTED_COLOR}}" onclick="selectChoice(this,{{$nestedChoiceItem->ID}},0,{{$questionSubItem->ID}})">{{$nestedChoiceItem->ANSWER_ITEM}}</button>
                        @if(!empty($nestedChoiceItem->subAnswers()->get()) && count($nestedChoiceItem->subAnswers()->get())>0)
                        <span>

                        </span>
                        @endif
                      @elseif( in_array($nestedChoiceItem->FK_LKPQUE_ANS_TYP ,[1102,1104,1105]) )
                        ---
                      @endif
                    @endforeach
                  </span>
                  @endif
                </div>
				@php
				$questionHasButtonResponse = true;
				@endphp
				
                @elseif( in_array($choiceItem->FK_LKPQUE_ANS_TYP ,[1102,1104,1105]) )
                  @php
                  $textInputs[] = $choiceItem;
                  @endphp
                @endif
              @endforeach
            @endforeach
          @endforeach
        </div>
		@if($questionHasButtonResponse)
		<input type="hidden" autocomplete="off" data-questionid="{{$questionSubItem->ID}}" class="question_selected_choice" id="question_selected_choice_{{$questionSubItem->ID}}" name="question_selected_choice[{{$questionSubItem->ID}}]" value="{{$questionAnsweredChoice}}">
		@endif


        <div class="col-12">
        <div class="row text-inputs-container question_cont_{{$questionSubItem->ID}}">
        @foreach($textInputs as $choiceItem)
		  @php
		  $explodedResponderUserTypes = explode(',',$choiceItem->RESPONDER_USER_TYPES??"");
		  $explodedViewerUserTypes = explode(',',$choiceItem->VIEWER_USER_TYPES??"");
		  @endphp
		  @if( empty($choiceItem->RESPONDER_USER_TYPES) || (!empty($choiceItem->RESPONDER_USER_TYPES) && in_array(auth()->user()->user_type, $explodedResponderUserTypes)) )
			  @if($choiceItem->FK_LKPQUE_ANS_TYP==1104)
			  <!--
				documentation responses
			  -->
			  <!-- LOAD COMPONENT HERE -->

			  @else
			  <!-- LOAD TEXTAREA HERE -->
			  @endif
		  @elseif( empty($choiceItem->VIEWER_USER_TYPES) || (!empty($choiceItem->VIEWER_USER_TYPES) && in_array(auth()->user()->user_type, $explodedViewerUserTypes)) )
			  @if($choiceItem->FK_LKPQUE_ANS_TYP==1104)
			  <!--
				documentation responses
			  -->
			  <!-- LOAD COMPONENT HERE(DISABLED) -->

			  @else
			  <!-- LOAD TEXTAREA HERE(DISABLED) -->
			  @endif
		  @endif
        @endforeach
        </div>
        </div>



      </div>
      @endforeach
      </div>
      @endif




    </div>
  </div>
@endforeach
@endforeach
</div>
</form>

<div class="row">
  <div class="col-12">
    <div id="footer-container">
      <div class="row">
        <div class="col-6 text-end">
          <button type="button" class="btn btn-success" onclick="storeCategoryChecklist()">ثبت</button>
        </div>
        <div class="col-6 text-start">
          <button type="button" class="btn btn-danger" onclick="redirectToPreviousCategory()">بازگشت</button>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection

@section('bottom-js-scripts')

<script src="{{asset('libs/datepicker/persian-date.js')}}"></script>
<script src="{{asset('libs/datepicker/js/persianDatepicker.min.js')}}"></script>
<script>
$(document).ready(function() {
	$(".j-date-calendar").persianDatepicker({
		formatDate: "YYYY-MM-DD",
		selectableYears: [1403,1404],
		selectedBefore: true
	});
});

function redirectToPreviousCategory(){


    $.ajax({
          url: '{{route("inspection.checklist.redirectToPreviousCategory")}}',
          type: 'POST',
          cache:false,
          data: {"brcode":{{$selectedBrCode}},"inspectionDate":'{{$inspectionDate}}', "categoryCode": '{{$categoryCode}}', "rental_box":'{{$rentalBox}}' },
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
          success: function(data){

              if(data.response_code==200){
                window.location.href = data.next_url;
              }


          },
          error: function(data){

              alert('ثبت با خطا مواجه شد!');

          }
      });
    
}


function storeCategoryChecklist(){

  $(".question-group-container").trigger("click");

  const forms = document.querySelectorAll('#checklist-questions-form');
  const form = forms[0];

  var hasError = undefined;

  var selectedChoices = [];
  Array.from(form.elements).forEach((input) => {
    if(input.type!='hidden' || $(input).hasClass('uploaded-file-container-input') ){
      return;
    }
	

    if(($(input).val()==undefined || $(input).val()==null || $(input).val()=='') && (typeof hasError == 'undefined')){
	  alert("توجه: شما به همه سوالات این سرفصل پاسخ نداده اید.!");
      hasError=false; //temporary disabled
    }

    selectedChoices[$(input).data('questionid')] = $(input).val();
  });


  var filledTextareas = [];

  //initialize array
  Array.from(form.elements).forEach((input) => {
    if(input.type!='textarea' && input.type!='number'){
      return;
    }

    filledTextareas[$(input).data('questionid')]= [];
  });

  Array.from(form.elements).forEach((input) => {
    if(input.type!='textarea' && input.type!='number'){
      return;
    }
	if( $(input).val() !== null && $(input).val()!==""){
		filledTextareas[$(input).data('questionid')].push([$(input).data('questionid'),$(input).data('choiceid'),$(input).val()]);
	}
  });
  
  
  //check values
  var value1;
  var value2;
  var notCheckedGrather = false;
  var questionWhichHasGratherError = 0;
  filledTextareas.forEach(function(input, qNum){
  
	value1=0;
	value2=0;
  
	input.forEach(function(answerArray, answerNum){
	
		if(answerArray[1]==21){
			value1 = answerArray[2];
		}else if(answerArray[1]==20){
			value2 = answerArray[2];
		}
	});
	
	if(parseInt(value1)>parseInt(value2)){
		notCheckedGrather = true;
		questionWhichHasGratherError = qNum;
		return;
	}
	
  });
  $('.has-error').removeClass('has-error');
  if(notCheckedGrather){
	if(questionWhichHasGratherError>0){
		$('#question_response_'+questionWhichHasGratherError+'_21').addClass('has-error');
		
		var scrollDiv = document.getElementById('question_response_'+questionWhichHasGratherError+'_21').offsetTop;
		window.scrollTo({ top: scrollDiv, behavior: 'smooth'});
	}
	return;
  }
  
  



  var filledDocuments = [];

  //initialize array
  Array.from(form.elements).forEach((input) => {
    if($(input).data('element-type')!='doc'){
      return;
    }

    filledDocuments[$(input).data('questionid')]= [];
  });

  
  documentsRequiredError = false;
  Array.from(form.elements).forEach((input) => {
    if($(input).data('element-type')!='doc'){
      return;
    }
	
	if( $(input).val() !== null && $(input).val()!==""){
		filledDocuments[$(input).data('questionid')].push([$(input).data('choiceid'),$(input).data('index'),[$(input).data('doc-part'),$(input).val()]]);
	}else{
		if($(input).is(":visible")){
		
			if($(input).data('doc-part') != 'brboss_desc' && $(input).data('doc-part') != 'personnel_code' && $(input).data('doc-part') != 'doc_number' && $(input).data('doc-part') != 'doc_date' && $(input).data('doc-part') != 'description' && $(input).data('doc-part') != 'documentation_file'){
				hasError = true;
				documentsRequiredError = true;
			}
		
		}
	}
    
  });
  
  if(documentsRequiredError){
	alert("خطا: وارد کردن تمامی مستندات مربوط به موارد رعایت نشده، اجباری است.");
  }



  if(!hasError){
    //pass selectedChoices, filledTextareas to the server
    $.ajax({
          url: '{{route("inspection.checklist.storeAnswers")}}',
          type: 'POST',
          cache:false,
          data: {"brcode":{{$selectedBrCode}},"inspectionDate":'{{$inspectionDate}}', "categoryCode": '{{$categoryCode}}',"selectedChoices":selectedChoices, "filledTextareas":filledTextareas, "filledDocuments":filledDocuments, "rental_box":'{{$rentalBox}}' },
          headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
          success: function(data){

              if(data.response_code==200){
                window.location.href = data.next_url;
              }


          },
          error: function(data){

              alert('ثبت با خطا مواجه شد!');

          }
      });
    }


}



function controlInputNumbers(textObject,questionID,choiceID){
	
	$(textObject).removeClass('has-error');
	
	if($('#question_response_'+questionID+'_20').val()!="" && $('#question_response_'+questionID+'_20').val()==0){
		$('#question_response_'+questionID+'_21').val(0);
	}else if($('#question_response_'+questionID+'_21').val()!="" && $('#question_response_'+questionID+'_20').val()!="" && parseInt($('#question_response_'+questionID+'_21').val(),0) > parseInt($('#question_response_'+questionID+'_20').val(),0)){
		$('#question_response_'+questionID+'_21').val($('#question_response_'+questionID+'_20').val());
	}
}


function documentCountsChanged(textObject,questionID,choiceID){





	if($('#question_response_'+questionID+'_21').val() && ($('#question_response_'+questionID+'_21').val()==0) ){ //fully observed
	
		//trigger fully observed button	
		if($(textObject).closest('.question-row').find('.question_selected_choice').val() != 13){
			$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked','0');
			$(textObject).closest('.question-row').find('*[data-buttonType="fully-observed"]').trigger('click');
			$('.documentation_inputs_container_'+questionID).addClass('hide');
			
			$('#sub-choices-container-'+questionID+'-'+choiceID).addClass('hide');
			
			
		}
		
	}else if($('#question_response_'+questionID+'_21').val() && $('#question_response_'+questionID+'_20').val() && (parseInt($('#question_response_'+questionID+'_21').val(),20)<parseInt($('#question_response_'+questionID+'_20').val(),20)) ){ //partially-observed
		
		//trigger partiallly-observed button
		var previousClickedValue = $(textObject).closest('.question-row').find('.question_selected_choice').val();

		if(previousClickedValue != 15){
			
			if($(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked')!==1){
				$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').trigger('click');
				$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked','1');
			}
			$(textObject).closest('.question-row').find('*[data-buttonType="partially-observed"]').trigger('click');
			$(textObject).closest('.question-row').find('*[data-buttonType="partially-observed"]').data('clicked','1');
			$('.documentation_inputs_container_'+questionID).removeClass('hide');
		}
			
		var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
		
		for(var i=(1+latestQuestionDocCount); i<=($('#question_response_'+questionID+'_21').val()); i++){
			addNewDocumentEntryRow($('#add_new_doc_btn_'+questionID), questionID, 19);
		}
		
		var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
		for(var i=1; i<=Math.abs(Math.abs($('#question_response_'+questionID+'_21').val())-latestQuestionDocCount); i++){
			$('.question_cont_'+questionID).find('.doc-row-btn-delete:last').trigger('click');
		}
		
			
		
		
		$('#sub-choices-container-'+questionID+'-'+$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('choiceid')).removeClass('hide');
		
	}else if($('#question_response_'+questionID+'_21').val() && $('#question_response_'+questionID+'_20').val() && (parseInt($('#question_response_'+questionID+'_21').val(),20)==parseInt($('#question_response_'+questionID+'_20').val(),20)) ){ //not-observed
	
	
		//trigger not-observed function	
		var previousClickedValue = $(textObject).closest('.question-row').find('.question_selected_choice').val();
		
		if(previousClickedValue != 16){
		
		
			if($(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked')!==1){
				$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').trigger('click');
				$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked','1');
			}
			
			$(textObject).closest('.question-row').find('*[data-buttonType="not-observed"]').trigger('click');
			$(textObject).closest('.question-row').find('*[data-buttonType="not-observed"]').data('clicked','1');
			$('.documentation_inputs_container_'+questionID).removeClass('hide');
			
			var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
			for(var i=(1+latestQuestionDocCount); i<=($('#question_response_'+questionID+'_21').val()); i++){
				addNewDocumentEntryRow($('#add_new_doc_btn_'+questionID), questionID, 19);
			}
			
			var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
			
			for(var i=1; i<=Math.abs(Math.abs($('#question_response_'+questionID+'_21').val())-latestQuestionDocCount); i++){
				$('.question_cont_'+questionID).find('.doc-row-btn-delete:last').trigger('click');
			}
			
		}
		
		$('#sub-choices-container-'+questionID+'-'+$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('choiceid')).removeClass('hide');
		
	}else{
	

		//set initial value
		
		
		if($(textObject).closest('.question-row').find('*[data-buttonType="not-observed"]').data('clicked')==1){
			$(textObject).closest('.question-row').find('*[data-buttonType="not-observed"]').trigger('click');
		}
		$(textObject).closest('.question-row').find('*[data-buttonType="not-observed"]').data('clicked','0');
		
		if($(textObject).closest('.question-row').find('*[data-buttonType="partially-observed"]').data('clicked')==1){
			$(textObject).closest('.question-row').find('*[data-buttonType="partially-observed"]').trigger('click');
		}
		$(textObject).closest('.question-row').find('*[data-buttonType="partially-observed"]').data('clicked','0');
		
		
		
		if($(textObject).closest('.question-row').find('*[data-buttonType="fully-observed"]').data('clicked')==1){
			$(textObject).closest('.question-row').find('*[data-buttonType=fully-observed"]').trigger('click');
		}
		$(textObject).closest('.question-row').find('*[data-buttonType="fully-observed"]').data('clicked','0');
		
		
		
		if($(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked')==1){
			$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').trigger('click');
		}
		$(textObject).closest('.question-row').find('*[data-buttonType="not-observed-main"]').data('clicked','0');
		
		
		
		$(textObject).closest('.question-row').find('.question_selected_choice').val("")
		
		
		$(textObject).closest('.question-row').find('.all-buttons-container').find('button').each(function( index ) {
			$( this ).removeClass('btn-selected-success');
			$( this ).removeClass('btn-selected-danger');
			$( this ).removeClass('btn-selected-warning');
		});

		
		if($('#question_response_'+questionID+'_21').val()>0){
			$('.documentation_inputs_container_'+questionID).removeClass('hide');
			
			var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
			for(var i=(1+latestQuestionDocCount); i<=($('#question_response_'+questionID+'_21').val()); i++){
				
				addNewDocumentEntryRow($('#add_new_doc_btn_'+questionID), questionID, 19);
			}
			
			var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');
			
			for(var i=1; i<=Math.abs(Math.abs($('#question_response_'+questionID+'_21').val())-latestQuestionDocCount); i++){
				$('.question_cont_'+questionID).find('.doc-row-btn-delete:last').trigger('click');
			}
		}else{
			$('.documentation_inputs_container_'+questionID).addClass('hide');
		}
	}

}


function openSubQuestions(obj, questionItemID){
  if($(obj).hasClass('down')){

    $('#sub-question-container-'+questionItemID).slideDown( 100, function() {
      //$('#sub-question-container-'+questionItemID).removeClass('hide');
    });
    $(obj).removeClass('down');
    $(obj).addClass('up');


  }else{

    $('#sub-question-container-'+questionItemID).slideUp( 100, function() {
      //$('#sub-question-container-'+questionItemID).addClass('hide');
    });

    $(obj).removeClass('up');
    $(obj).addClass('down');

  }
}

function openSubChoiceContainer(questionID, answerItemID){
  @php
	if($userType=="BRANCH_BOSS"){
		echo "return;";
	}
	if($disabledFields){
		echo "return;";
	}
  @endphp
  if($('#sub-choices-container-'+questionID+'-'+answerItemID).hasClass('hide')){
    $('#sub-choices-container-'+questionID+'-'+answerItemID).removeClass('hide');
  }else{
    $('#sub-choices-container-'+questionID+'-'+answerItemID).addClass('hide')
  }


}




function selectChoice(obj, choiceItemID, subAnswersCount, questionID){

  @php
	if($userType=="BRANCH_BOSS"){
		echo "return;";
	}
	
	if($disabledFields){
		echo "return;";
	}
  @endphp

  $(obj).closest('.all-buttons-container').find('button').each(function( index ) {
    $( this ).removeClass('btn-selected-success');
    $( this ).removeClass('btn-selected-danger');
    $( this ).removeClass('btn-selected-warning');
  });
  $(obj).addClass($(obj).data('selectedcolor'));

  $('#question_selected_choice_'+questionID).val(choiceItemID);
}


function eliminateDocRow(questionID, choiceID, rowNum){

	var latestQuestionDocCount = $('#add_new_doc_btn_'+questionID).data('latest_count');

	$('#question_'+questionID+'_doc_row_'+choiceID+'_'+rowNum).remove();
	
	
	latestQuestionDocCount = latestQuestionDocCount-1;
	$('#add_new_doc_btn_'+questionID).data('latest_count',latestQuestionDocCount)
}

function enforceMinMax(el) {
  if (el.value != "") {
    if (parseInt(el.value) < parseInt(el.min)) {
      el.value = el.min;
    }
    if (parseInt(el.value) > parseInt(el.max)) {
      el.value = el.max;
    }
  }
}




$(function () {
	$(".documentation_file_input").on('change', uploadFileFunction);
});
</script>
@endsection


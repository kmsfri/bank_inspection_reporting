<!DOCTYPE html>
    <html lang="fa">
    <head>
        <meta charset="UTF-8">
        <title>خلاصه وضعیت بازرسی</title>
        <style>
            body {
                font-family: "vazir", sans-serif;
            }
			h5{
				font-size: 16px;
				font-weight: 700;
			}
			table{
				direction: rtl;
			}
			td, th{
				font-size: 11px !important;
				text-align: right;
			}
        </style>
    </head>
    <body>
        <table id="final-page-overal-info" class="table mb-5">
	<thead class="thead-dark"><tr><th colspan="6" class="text-end"><h5>خلاصه وضعیت بازرسی</h5></th></tr></thead>
	<tbody>
		<tr>
			<td class="border-right">
			نام شعبه:
			</td>
			<td class="border-left font-bold">
			{{$selectedBrName}}
			</td>
			<td>
			کد شعبه:
			</td>
			<td class="border-left font-bold">
			{{$selectedBrCode}}
			</td>
			<td>
			درجه شعبه:
			</td>
			<td class="font-bold border-left">
			{{$darajehTitle}}
			</td>
		</tr>
		
		<tr>
			<td class="border-right">
			نام حوزه تحت پوشش:
			</td>
			<td class="border-left font-bold">
			{{$branchclassification->bazhozename}}
			</td>
			<td>
			نام امور شعب:
			</td>
			<td class="border-left font-bold">
			{{$branchclassification->sarname}}
			</td>
			<td>
			نام رئیس شعبه:
			</td>
			<td class="font-bold border-left">
			{{$branchBoss}}
			</td>
		</tr>
		<tr>
			
		</tr>
		<tr>
			<td class="border-right">
			امتیاز:
			</td>
			<td class="border-left font-bold">
			{{$scoreStatusArray['avg']}}
			</td>
			<td>
			وضعیت بازرسی:
			</td>
			<td class="border-left font-bold pt-2">
				<span class="{{$scoreStatusArray['style_class']}} final-page-badge">
					{{$scoreStatusArray['title']}}
				<span>
			</td>
			<td>
			نام بازرس:
			</td>
			<td class="font-bold border-left">
			{!! implode('</br>',$inspectors) !!}
			</td>
		</tr>
		<tr>
			
		</tr>
		<tr>
			<td colspan="1" class="border-right">
			تاریخ شروع بازرسی:
			</td>
			<td colspan="1" class="border-left font-bold">
			{{$inspectionStartDate}}
			</td>
			<td colspan="1">
			تاریخ پایان بازرسی:
			</td>
			<td colspan="3" class="border-left font-bold">
			{{$inspectionEndDate}}
			</td>
		</tr>
		
		<tr>
			<td colspan="1" class="border-right">
			شماره حکم بازرسی:
			</td>
			<td colspan="1" class="border-left font-bold">
			{{$inspectorProgramNumber}}
			</td>
			<td colspan="1">
			تاریخ حکم بازرسی:
			</td>
			<td colspan="3" class="border-left font-bold">
			{{$inspectorProgramDate}}
			</td>
		</tr>
		
		<tr>
			<td colspan="1" class="border-right">
			توضیحات بازرس:
			</td>
			<td colspan="5" class="font-bold">
			{{$inspectorResponse}}
			</td>
		</tr>
		
	</tbody>
	</table>


	<table id="final-page-scores-info" class="table mb-5">
	<thead class="thead-dark">
		<tr><th colspan="5" class="text-end"><h5>خلاصه وضعیت امتیاز سرفصل ها</h5></th></tr>
		<tr>
			<th>گروه</th>
			<th>فصل</th>
			<th>تعداد سوالات</th>
			<th>رعایت نشده / بررسی شده</th>
			<th>امتیاز گروه</th>
		</tr>
	</thead>
	<tbody>
		@foreach($questionGroupSummaryStats as $summaryRow)
		<tr>
			<td colspan="1">
				{{$summaryRow['group_title']}}
			</td>
			<td colspan="1">
				{{$summaryRow['category_title']}}
			</td>
			<td colspan="1">
				{{$summaryRow['group_questions_count']}}
			</td>
			<td colspan="1">
				{{$summaryRow['reviewed_to_total_ratio']}}
			</td>
			<td colspan="1">
				{{$summaryRow['category_total_score_value']}}
			</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td class="font-bold">جمع کل</td>
			<td class="font-bold">{{$questionGroupSummaryStatsSum['all_question_groups_count']}}</td>
			<td class="font-bold">{{$questionGroupSummaryStatsSum['group_questions_count']}}</td>
			<td class="font-bold">{{$questionGroupSummaryStatsSum['reviewed_to_total_ratio']}}</td>
			<td class="font-bold">{{$questionGroupSummaryStatsSum['category_total_score_value']}}</td>
		</tr>
	</tfoot>
	</table>


	<table id="final-page-table" class="table table-striped table-hover">
		<thead><tr><th style="width:60%" class="text-end">سوال</th><th style="width:30%">پاسخ ها</th><th style="width:10%">مستندات</th></tr></thead>
		<tbody>
			@foreach($checklistArray as $checklistItem)
			
			@php
			$documentString = "";
			$hasDocTitle = "ندارد";
			if(count($checklistItem['questionAnswerDocuments'])>0){
				$hasDocTitle = count($checklistItem['questionAnswerDocuments'])." مورد دارد";
			}
			
			if(!empty($checklistItem['questionAnswerDocuments']) && count($checklistItem['questionAnswerDocuments'])>0){
				$counter = 1;
				foreach($checklistItem['questionAnswerDocuments'] as $docRow){

					$decodedJson = !empty($docRow->DOCUMENTATION)?json_decode($docRow->DOCUMENTATION,true):[];
					if(count($decodedJson)>0){
					
						if($counter>1){
							$documentString .= "\n\n";
						}
					
						$documentString .= 	"مورد ".$counter.":\n"
											."شماره حساب: "
											.(isset($decodedJson['account_number'])?$decodedJson['account_number']:'-')
											."\nکد ملی: "
											.(isset($decodedJson['national_code'])?$decodedJson['national_code']:'-')
											."\nنام و نام خانوادگی: "
											.(isset($decodedJson['customer_fullname'])?$decodedJson['customer_fullname']:'-')
											."\nکد پرسنلی: "
											.(isset($decodedJson['personnel_code'])?$decodedJson['personnel_code']:'-')
											."\nکد پرسنلی: "
											.(isset($decodedJson['personnel_code'])?$decodedJson['personnel_code']:'-')
											."\nشماره سند: "
											.(isset($decodedJson['doc_number'])?$decodedJson['doc_number']:'-')
											."\nتاریخ سند: "
											.(isset($decodedJson['doc_date'])?$decodedJson['doc_date']:'-')
											."\nتوضیحات: "
											.(isset($decodedJson['description'])?$decodedJson['description']:'-');
						
						$counter++;
											
											
											
					}
				}
				
			}
			@endphp
			
			<tr>
				<td class="text-end">{{$checklistItem['questionText']->QUESTION_NO_TITLE}}&nbsp;&nbsp;{{$checklistItem['questionText']->QUESTION}}</td>
				<td>
					@foreach($checklistItem['questionAnswerTexts'] as $answerTextItem)
						{{$answerTextItem->AnswerItem()->first()->ANSWER_ITEM}}: {{$answerTextItem->ANSWER_TEXT}}</br>
					@endforeach
					
					@if($checklistItem['questionText']->FK_LKPQUE_TYPEQUE==1203)
						وضعیت: {{$checklistItem['questionAnswerChoices']->ANSWER_ITEM}}</br>
					@else
						{{$checklistItem['questionAnswerChoices']->ANSWER_ITEM}}
					@endif
					
				</td>
				<td>
					@if(count($checklistItem['questionAnswerDocuments'])>0)
					<a data-toggle="tooltip" title="{!! $documentString !!}" href="javascript::void();">{{$hasDocTitle}}</a>
					@else
					{{$hasDocTitle}}
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
    </body>
    </html>

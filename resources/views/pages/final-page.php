@extends('layout.master')
@section('top-libraries')
<link rel="stylesheet" href="{{asset('libs/datepicker/css/persianDatepicker-default.css')}}" />

@endsection
@section('contents')

<div class="row" id="select-branch-container">


  @if (\Session::has('message'))
  <div class="alert alert-success">
  <ul>
	  <li>{!! \Session::get('message') !!}</li>
  </ul>
  </div>
  @endif

  @if($message)
  <form action="{{route('inspection.checklist.startChecklist')}}" method="post" class="col-5">
    {{csrf_field()}}
	<div class="row">
      <div class="col-12 float-start">
        
		<div class="alert alert-success" role="alert">
		  {{$message}}
		</div>		
      </div>
    </div>
	
  </form>
  @endif
  
  
  
  <div class="col-12">
	<div class="row">
      <div class="col-12 float-start">
		<div id="final-page-table-wrapper" class="table-responsive">
		
	
		<table id="final-page-overal-info" class="table mb-5">
		<thead class="thead-dark"><tr><th colspan="6" class="text-end"><h5>خلاصه وضعیت بازرسی</h5></th></tr></thead>
		<tbody>
			<tr>
				<td class="border-right">
				نام شعبه:
				</td>
				<td class="border-left font-bold">
				{{$selectedBrName}}
				</td>
				<td>
				کد شعبه:
				</td>
				<td class="border-left font-bold">
				{{$selectedBrCode}}
				</td>
				<td>
				درجه شعبه:
				</td>
				<td class="font-bold border-left">
				{{$darajehTitle}}
				</td>
			</tr>
			
			<tr>
				<td class="border-right">
				نام حوزه تحت پوشش:
				</td>
				<td class="border-left font-bold">
				{{$branchclassification->bazhozename}}
				</td>
				<td>
				نام امور شعب:
				</td>
				<td class="border-left font-bold">
				{{$branchclassification->sarname}}
				</td>
				<td>
				نام رئیس شعبه:
				</td>
				<td class="font-bold border-left">
				{{$branchBoss}}
				</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td class="border-right">
				امتیاز:
				</td>
				<td class="border-left font-bold">
				{{$scoreStatusArray['avg']}}
				</td>
				<td>
				وضعیت بازرسی:
				</td>
				<td class="border-left font-bold pt-2">
					<span class="{{$scoreStatusArray['style_class']}} final-page-badge">
						{{$scoreStatusArray['title']}}
					<span>
				</td>
				<td>
				نام بازرس:
				</td>
				<td class="font-bold border-left">
				{!! implode('</br>',$inspectors) !!}
				</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td colspan="1" class="border-right">
				تاریخ شروع بازرسی:
				</td>
				<td colspan="1" class="border-left font-bold">
				{{$inspectionStartDate}}
				</td>
				<td colspan="1">
				تاریخ پایان بازرسی:
				</td>
				<td colspan="3" class="border-left font-bold">
				{{$inspectionEndDate}}
				</td>
			</tr>
			
			<tr>
				<td colspan="1" class="border-right">
				شماره حکم بازرسی:
				</td>
				<td colspan="1" class="border-left font-bold">
				{{$inspectorProgramNumber}}
				</td>
				<td colspan="1">
				تاریخ حکم بازرسی:
				</td>
				<td colspan="3" class="border-left font-bold">
				{{$inspectorProgramDate}}
				</td>
			</tr>
			
			<tr>
				<td colspan="1" class="border-right">
				توضیحات بازرس:
				</td>
				<td colspan="5" class="font-bold">
				{{$inspectorResponse}}
				</td>
			</tr>
			
		</tbody>
		</table>
		
		
		<table id="final-page-scores-info" class="table mb-5">
		<thead class="thead-dark">
			<tr><th colspan="5" class="text-end"><h5>خلاصه وضعیت امتیاز سرفصل ها</h5></th></tr>
			<tr>
				<th>گروه</th>
				<th>فصل</th>
				<th>تعداد سوالات</th>
				<th>رعایت نشده / بررسی شده</th>
				<th>امتیاز گروه</th>
			</tr>
		</thead>
		<tbody>
			@foreach($questionGroupSummaryStats as $summaryRow)
			<tr>
				<td colspan="1">
					{{$summaryRow['group_title']}}
				</td>
				<td colspan="1">
					{{$summaryRow['category_title']}}
				</td>
				<td colspan="1">
					{{$summaryRow['group_questions_count']}}
				</td>
				<td colspan="1">
					{{$summaryRow['reviewed_to_total_ratio']}}
				</td>
				<td colspan="1">
					{{$summaryRow['category_total_score_value']}}
				</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td class="font-bold">جمع کل</td>
				<td class="font-bold">{{$questionGroupSummaryStatsSum['all_question_groups_count']}}</td>
				<td class="font-bold">{{$questionGroupSummaryStatsSum['group_questions_count']}}</td>
				<td class="font-bold">{{$questionGroupSummaryStatsSum['reviewed_to_total_ratio']}}</td>
				<td class="font-bold">{{$questionGroupSummaryStatsSum['category_total_score_value']}}</td>
			</tr>
		</tfoot>
		</table>
		
		
		<table id="final-page-table" class="table table-striped table-hover">
			<thead><tr><th style="width:60%" class="text-end">سوال</th><th style="width:30%">پاسخ ها</th><th style="width:10%">مستندات</th></tr></thead>
			<tbody>
				@foreach($checklistArray as $checklistItem)
				
				@php
				$documentString = "";
				$documentَArray = [];
				$hasDocTitle = "ندارد";
				if(count($checklistItem['questionAnswerDocuments'])>0){
					$hasDocTitle = count($checklistItem['questionAnswerDocuments'])." مورد دارد";
				}
				
				if(!empty($checklistItem['questionAnswerDocuments']) && count($checklistItem['questionAnswerDocuments'])>0){
					$counter = 1;
					foreach($checklistItem['questionAnswerDocuments'] as $docRow){

						$decodedJson = !empty($docRow->DOCUMENTATION)?json_decode($docRow->DOCUMENTATION,true):[];
						if(count($decodedJson)>0){
						
							$documentَArray[] = [
								'index' => $counter,
								'account_number' => $decodedJson['account_number']??'',
								'national_code' => $decodedJson['national_code']??'',
								'customer_fullname' => $decodedJson['customer_fullname']??'',
								'personnel_code' => $decodedJson['personnel_code']??'',
								'doc_number' => $decodedJson['doc_number']??'',
								'doc_date' => $decodedJson['doc_date']??'',
								'description' => $decodedJson['description']??''
							];
							
							$counter++;
												
												
												
						}
					}
					
				}
				@endphp
				
				<tr>
					<td class="text-end">{{$checklistItem['questionText']->QUESTION_NO_TITLE}}&nbsp;&nbsp;{{$checklistItem['questionText']->QUESTION}}</td>
					<td>
						@foreach($checklistItem['questionAnswerTexts'] as $answerTextItem)
							{{$answerTextItem->AnswerItem()->first()->ANSWER_ITEM}}: {{$answerTextItem->ANSWER_TEXT}}</br>
						@endforeach
						
						@if($checklistItem['questionText']->FK_LKPQUE_TYPEQUE==1203)
							وضعیت: {{$checklistItem['questionAnswerChoices']->ANSWER_ITEM}}</br>
						@else
							{{$checklistItem['questionAnswerChoices']->ANSWER_ITEM}}
						@endif
						
					</td>
					<td>
						@if(count($checklistItem['questionAnswerDocuments'])>0)
						<a href="javascript:void(0)" onclick="showDocumentsDetailsModal('{{ json_encode($documentَArray) }}')">{{$hasDocTitle}}</a>
						@else
						{{$hasDocTitle}}
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
		
	  </div>
	</div>
	
	<div class="row">
      <div class="col-12 text-center">
		<button class="btn btn-success" onclick="printDiv('final-page-table-wrapper')">چاپ گزارش</button>
		<a class="btn btn-success" href="{{$exportPDF_url}}">خروجی PDF</a>
	  </div>
	</div>
	
  </div>


</div>

<div id="document_details_modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">مستندات ثبت شده</h5>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" onclick="$('#document_details_modal').modal('hide')" class="btn btn-secondary" data-dismiss="modal">بستن</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('bottom-js-scripts')
<script>
	function showDocumentsDetailsModal(data){
	
		
	
		var data_array =  JSON.parse(data);
		
		var appendHTML = "<table class='table table-striped table-hover'><thead class='table-dark'><tr><th>ردیف</th><th>شماره حساب</th><th>کد ملی</th><th>نام و نام خانوادگی</th><th>کد پرسنلی</th><th>شماره سند</th><th>تاریخ سند</th><th>توضیحات</th></tr></thead>";
		appendHTML += "<tbody>";
		data_array.forEach((row_item) => {
		  appendHTML += "<tr>";
			appendHTML += "<td>"+row_item.index+"</td>";
			appendHTML += "<td>"+row_item.account_number+"</td>";
			appendHTML += "<td>"+row_item.national_code+"</td>";
			appendHTML += "<td>"+row_item.customer_fullname+"</td>";
			appendHTML += "<td>"+row_item.personnel_code+"</td>";
			appendHTML += "<td>"+row_item.doc_number+"</td>";
			appendHTML += "<td>"+row_item.doc_date+"</td>";
			appendHTML += "<td>"+row_item.description+"</td>";
		  appendHTML += "</tr>";
		});
		appendHTML += "</tbody>";
		appendHTML += "<table>";
		
		$('#document_details_modal .modal-body').html(appendHTML);
		
		$('#document_details_modal').modal('show');
	
	}
</script>
@endsection


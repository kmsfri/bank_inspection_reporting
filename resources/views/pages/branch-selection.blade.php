@extends('layout.master')
@section('top-libraries')
<link rel="stylesheet" href="{{asset('libs/datepicker/css/persianDatepicker-default.css')}}" />
<link href="{{asset('libs/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('contents')

<div class="row" id="select-branch-container">

  <form id="branchSelectionForm" action="{{route('inspection.checklist.startChecklist')}}" method="post" class="col-7">

    {{csrf_field()}}
	
    <div class="row">
      <div class="col-4 float-end"> 
        <label for="brcode" class="form-label">کد شعبه:</label>
      </div>

      <div class="col-8 float-start">
        <select class="form-select" name="brcode" id="brcode" onChange="changeBranchesList();">
		
			<option value="" selected disabled>انتخاب کنید</option> 
			@foreach($branchesList as $branchItem)
			<option {{($branchItem['brcode']==$selectedBranchCode)?'selected':''}} data-arzi="{{$branchItem['arzi']}}" data-rental="{{$branchItem['rental']}}" value="{{$branchItem['brcode']}}">شعبه {{$branchItem['brname']."(".$branchItem['brcode'].") ".( !empty($branchItem['sarcode'] && $branchItem['sarcode']>0)?" - اداره امور ".$branchItem['sarcode']:'')}}</option>
			@endforeach
		
		</select>
      </div>
    </div>

    <div class="row mt-2">
      <div class="col-4 float-end">
        <label for="inspection_date" class="form-label">تاریخ بازرسی:</label>
      </div>

      <div class="col-8 float-start">
        <input type="text" class="form-control text-start dir-ltr" name="inspection_date" id="inspection_date" placeholder="1402-11-07">
      </div>
    </div>
	
	<div class="row mt-2">
	  <div class="col-4 float-end">
        &nbsp;
      </div>
      <div class="col-8 float-start">
        <div class="form-check">
		  <input class="form-check-input float-end" type="checkbox" value="" disabled id="arzi_checkbox" name="arzi_checkbox">
		  <label class="form-check-label" for="arzi_checkbox">
			شعبه ارزی
		  </label>
		</div>
		<div class="form-check">
		  <input class="form-check-input float-end" type="checkbox" value="1" id="rental_box" name="rental_box">
		  <label class="form-check-label float-end" for="rental_box">
			دارای صندوق اجاره ای
		  </label>
		</div>
      </div>
    </div>
	
    <div class="row mt-4">
      <div class="col-12 text-start">
        <button type="button" class="btn btn-sm btn-success" onClick="openSelectBranchModal()">جستجو</button>
        <button type="button" class="btn btn-sm btn-danger">پاک کردن</button>
      </div>
    </div>

  </form>


</div>
<div id="branchSelectionModal" class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		<div class="row">
			<div class="col-12">
				<button type="button" class="btn btn-danger" onClick="doNewInspection()">ثبت بازرسی جدید</button>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col-12">
				<button type="button" class="btn btn-success" onclick="alert('سابقه ی بازرسی برای این شعبه یافت نشد!')">مشاهده سوابق</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>


@endsection

@section('bottom-js-scripts')
<script src="{{asset('libs/datepicker/persian-date.js')}}"></script>
<script src="{{asset('libs/datepicker/js/persianDatepicker.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
  
  
	var p = new persianDate();
    var today = p.now().toString("YYYY-MM-DD");
  
    $("#inspection_date").persianDatepicker({
        formatDate: "YYYY-MM-DD",
        selectableYears: [1403],
		selectedDate: 'today',
		startDate: '1403/01/01',
		endDate: "today"
    });
	
	
	$("#inspection_date").val(today);
	
  });
  
  
  
  
  
  function openSelectBranchModal(){
  
	var e = document.getElementById('brcode');
	var selectedBrCode = $(e.options[e.selectedIndex]).val();
	
	var inspectionDate = $("#inspection_date").val();
	
	if(selectedBrCode=="" || selectedBrCode==undefined || selectedBrCode==null || 
		inspectionDate=="" || inspectionDate==undefined || inspectionDate==null){
		alert("لطفا شعبه و تاریخ بازرسی را انتخاب کنید!");
		return false;
	}
  
  
	$('#branchSelectionModal').show();
  }
  
  
  $('#branchSelectionModal .btn-close').on('click',function(){
	$('#branchSelectionModal').hide();
  });
  
  
  function doNewInspection(){
	$('#branchSelectionForm').submit();
  }
  
  
  function changeBranchesList(){
  
	//set arzi checkbox enabled
	$('#arzi_checkbox').prop('checked', false);
	$('#rental_box').prop('checked', false);
  
	var e = document.getElementById('brcode');
	var arziStatus = $(e.options[e.selectedIndex]).data('arzi');
	if(arziStatus==1){
		//set arzi checkbox disabled
		$('#arzi_checkbox').prop('checked', true);
	}
	
	var rentalStatus = $(e.options[e.selectedIndex]).data('rental');
	if(rentalStatus==1){
		//set arzi checkbox disabled
		$('#rental_box').prop('checked', true);
	}

	
  
  
  }

  
  

	// In your Javascript (external .js resource or <script> tag)
	$(document).ready(function() {
		$('#brcode').select2({
			minimumInputLength: 2,
			"language": {
			   "noResults": function(){
				   return "هیچ شعبه ای یافت نشد!";
			   },
			   "inputTooShort": function(args){
				   return "برای جستجو حداقل 2 کاراکتر از کد یا نام شعبه را وارد کنید";
			   },
		   },
		});
	});

  
  
  
</script>
@endsection


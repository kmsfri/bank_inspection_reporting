@extends('layout.master')
@section('contents')

<div class="row" id="select-operation-container">
  @if(auth()->user()->user_type!="BRANCH_BOSS")
  <div class="col-12 text-center mt-5">
    <a href="{{route('inspection.checklist.branchSelection')}}" class="btn btn-danger">
      ثبت بازرسی جدید
    </a>
  </div>
  @endif
  
  <div class="col-12 text-center mt-3">
    <a href="{{route('inspection.report.show-reports-list')}}" class="btn btn-success">
      لیست گزارش ها
    </a>
  </div>
  
  
  <div class="col-12 text-center mt-3">
    <a href="{{route('inspection.resources.show-resources-list')}}" class="btn btn-warning">
      منابع
    </a>
  </div>
</div>


@endsection


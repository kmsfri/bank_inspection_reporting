@extends('layout.master')
@section('contents')
<div class="row justify-content-center">
	<div class="col-md-4">
		<div class="card">
			<h6 class="card-header text-center">ورود به چک لیست مبارزه با پولشویی</h6>
			<div class="card-body">
				<form method="POST" action="{{ route('login') }}">
					@csrf
					<div class="form-group mb-3">
						<input type="text" placeholder="نام کاربری" class="form-control" name="username" required autofocus>
					</div>

					<div class="form-group mb-3">
						<input type="password" placeholder="رمز عبور" class="form-control" name="password" required>
						@if ($errors->has('usernamePassword'))
						<span class="text-danger">{{ $errors->first('usernamePassword') }}</span>
						@endif
					</div>

					<div class="form-group mb-3">
						<div class="checkbox text-end">
							<label>
								<input type="checkbox" name="remember"> مرا به یاد داشته باش
							</label>
						</div>
					</div>

					<div class="d-grid mx-auto">
						<button type="submit" class="btn btn-dark btn-block">ورود</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>
@endsection

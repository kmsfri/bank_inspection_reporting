@php
$itHasPreviousRecord = false;
@endphp
@for($i = 0; $i<$countFrom; $i++)
	@php
	$selectedResponse = $documentsData[$i]??NULL;;
	if(!empty($selectedResponse)){
		$responseDecoded = json_decode($selectedResponse, true);
		if(!empty($responseDecoded['code_type']) || !empty($responseDecoded['national_code']) || !empty($responseDecoded['account_number']) || !empty($responseDecoded['description']) || !empty($responseDecoded['documentation_file'])){
			$itHasPreviousRecord = true;
			break;
		}
	}
	@endphp
@endfor
<div class="documentation_inputs_container_{{$questionItem->ID}} {{!$itHasPreviousRecord?'hide':''}}">
<div class="row">
  <div id="documentation_inputs_container_{{$questionItem->ID}}_{{$choiceItem->ID}}" class="col-12">
    @for($i = 0; $i<$countFrom; $i++)
	
	@php
	$selectedResponse = $documentsData[$i]??NULL;;
	if(empty($selectedResponse) && $i>0){
		continue;
	}
	$responseDecoded = [];
	if(!empty($selectedResponse)){
		$responseDecoded = json_decode($selectedResponse, true);
	}
	
	@endphp
    <div class="row {{($i>0)?'question_doc_el_row':''}}" id="question_{{$questionItem->ID}}_doc_row_{{$choiceItem->ID}}_{{$i}}">
      <div class="col-2 text-end dir-rtl form-group"> 
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_account_number">شماره حساب مشتری:</label>
        <input {{$disabled?'disabled':''}} onKeyUp="getNationalCodeByAccountNumber(this,'question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_national_code','question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_fullname','question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_customer_type')" type="text" value="{{ ($responseDecoded['account_number'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="account_number" class="form-control form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_account_number" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['account_number']">
      </div>
	  <div class="col-2 text-end dir-rtl form-group" id="doc_code_container_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_national_code">شماره ملی مشتری:</label>
        <input {{$disabled?'disabled':''}} disabled type="text" value="{{ ($responseDecoded['national_code'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="national_code" class="form-control doc-national-code form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_national_code" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['national_code']">
		<div class="national-code-alert-box alert alert-warning align-items-center" role="alert">
			کد وارد شده درست نیست!
		</div>
      </div>
	  <div class="col-3 text-end dir-rtl form-group" id="doc_code_container_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_fullname">نام و نام خانوادگی مشتری:</label>
        <input {{$disabled?'disabled':''}} disabled type="text" value="{{ ($responseDecoded['customer_fullname'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="customer_fullname" class="form-control doc-full-name form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_fullname" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['customer_fullname']">
      </div>
	  <div class="col-2 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_customer_type">نوع مشتری:</label>
		<select {{$disabled?'disabled':''}} disabled data-element-type="doc" data-index="{{$i}}" data-doc-part="customer_type"  data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_customer_type" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['customer_type']" class="form-select form-select-md text-center" aria-label="Default select example" autocomplete="off">
		  <option selected disabled>انتخاب کنید</option>
		  <option {{( ($responseDecoded["customer_type"] ?? NULL) =="REAL")?"selected":NULL}} value="REAL" {{(!empty($customerType) && $customerType!="REAL")?'disabled':''}}>حقیقی</option>
		  <option {{( ($responseDecoded["customer_type"] ?? NULL) =="LEGAL")?"selected":NULL}} value="LEGAL" {{(!empty($customerType) && $customerType!="LEGAL")?'disabled':''}}>حقوقی</option>
		</select>
      </div>
	  <div class="col-3 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_personnel_code">شماره استخدامی:</label>
        <input {{$disabled?'disabled':''}} onfocusout="getPersonnelByPersonnelCode(this,'question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_personnel_code','personnel_code_error_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_span')" type="text" value="{{ ($responseDecoded['personnel_code'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="personnel_code" class="form-control form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_description" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['personnel_code']">
		<span id="personnel_code_error_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_span"></span>
      </div>
	  <div class="col-1 text-end dir-rtl form-group hide">
        <label>&nbsp;</label>
        <button type="button" class="form-control btn btn-danger doc-row-btn-delete" onclick="eliminateDocRow({{$questionItem->ID}},{{$choiceItem->ID}},{{$i}})"><span class="close">&times;</span></button>
      </div>
	  <div class="col-2 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_doc_number">شماره سند:</label>
        <input {{$disabled?'disabled':''}} type="text" value="{{ ($responseDecoded['doc_number'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="doc_number" class="form-control form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_description" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['doc_number']">
      </div>
	  <div class="col-2 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_doc_date">تاریخ سند:</label>
        <input {{$disabled?'disabled':''}} type="text" value="{{ ($responseDecoded['doc_date'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="doc_date" class="form-control form-control-sm j-date-calendar" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_description" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['doc_date']">
      </div>
	  <div class="col-5 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_description">توضیحات:</label>
        <input {{$disabled?'disabled':''}} type="text" value="{{ ($responseDecoded['description'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="description" class="form-control form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_description" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['description']">
      </div>
      <div class="col-3 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_documentation_file">فایل مستندات:</label>
        <input {{$disabled?'disabled':''}} type="file"" data-index="{{$i}}" class="form-control documentation_file_input" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_documentation_file" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}">
		@if(!empty($responseDecoded['documentation_file']))
			<a href="{{asset($responseDecoded['documentation_file'])}}" target="_blank">مشاهده فایل</a>
		@endif
		<input {{$disabled?'disabled':''}} class="uploaded-file-container-input" type="hidden" value="{{ $responseDecoded['documentation_file'] ?? "" }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="documentation_file" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['documentation_file']" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_documentation_file">
      </div>
	  
	  
	  <div class="col-12 text-end dir-rtl form-group">
        <label for="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_brboss_desc">توضیحات رئیس شعبه:</label>
        <input {{$disabledBrBossFields?'disabled':''}} type="text" value="{{ ($responseDecoded['brboss_desc'] ?? "") }}" data-element-type="doc" data-index="{{$i}}" data-doc-part="brboss_desc" class="form-control form-control-sm" autocomplete="off" data-questionid="{{$questionItem->ID}}" data-choiceid="{{$choiceItem->ID}}" id="question_response_{{$questionItem->ID}}_{{$choiceItem->ID}}_{{$i}}_brboss_desc" name="question_filled_response[{{$questionItem->ID}}][{{$choiceItem->ID}}][{{$i}}]['brboss_desc']">
      </div>
    </div>
    @endfor
  </div>
</div>
<div class="row dir-ltr hide">
  <div class="col-1 form-group">
    <label>&nbsp;</label>
    <button id="add_new_doc_btn_{{$questionItem->ID}}" {{$disabled?'disabled':''}} type="button" data-latest_count="{{$countFrom}}" onclick="addNewDocumentEntryRow(this,{{$questionItem->ID}},{{$choiceItem->ID}})" class="form-control btn btn-success btn-sm">افزودن</button>
  </div>
</div>
</div>


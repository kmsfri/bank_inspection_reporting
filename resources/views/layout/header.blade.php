<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><img class="img-fluid" src="{{asset('static/images/bmi-logo.png')}}"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <span class="navbar-title text-end">سامانه نظارتی مبارزه با پولشویی</span>
	  @if(auth()->check())
      <form class="d-flex">
		<a class="btn btn-danger navbar-btn-logout" href="{{route('logout')}}">خروج</a>
		<span class="text-white ms-3">
		{{auth()->user()->fullname}}
		</span>
      </form>
	  @endif
    </div>
  </div>
</nav>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Bootstrap CSS -->
    <link href="{{asset('libs/bootstrap-5.0.2/bootstrap.min.css')}}" rel="stylesheet" crossorigin="anonymous">

    <link href="{{asset('libs/custom/styles.css?param='.time())}}" rel="stylesheet">

    @yield('top-libraries')

    <title>چک‌لیست مبارزه با پولشویی</title>
  </head>
  <body>

    @include('layout.header') 
    <div id="page-wrapper" class="container">
      @include('layout.heading')

      @yield('contents')

    </div>

    <script src="{{asset('libs/bootstrap-5.0.2/jquery.min.js')}}"></script>
    <script src="{{asset('libs/bootstrap-5.0.2/bootstrap.bundle.min.js')}}" crossorigin="anonymous"></script>
	
	<script src="{{asset('libs/select2/js/select2.min.js')}}"></script>
	
	
    <script>
    function addNewDocumentEntryRow(obj, questionID, choiceID){

	
      var latestCount = $(obj).data('latest_count');
	  
	  previousNumber = latestCount - 1;
	  
	  //check previous row fields
	  var previousNationalCode = $('#question_response_'+questionID+'_'+choiceID+'_'+previousNumber+'_national_code').val();
	  var previousAccountNumber = $('#question_response_'+questionID+'_'+choiceID+'_'+previousNumber+'_account_number').val();
	  var previousDescription = $('#question_response_'+questionID+'_'+choiceID+'_'+previousNumber+'_description').val();
	  
	  /*
	  if(previousNationalCode == "" || previousAccountNumber == "" || previousDescription == ""){
		alert("لطفا ابتدا فیلد های ردیف قبل را پر کنید!");
		return;
	  }
	  */
	  

      var rowHTML = '\
      <div class="row question_doc_el_row" id="question_'+questionID+'_doc_row_'+choiceID+'_'+latestCount+'">\
        <div class="col-2 text-end dir-rtl form-group">\
          <label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_account_number">شماره حساب مشتری:</label>\
          <input type="text" onKeyUp="getNationalCodeByAccountNumber(this,\'question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_national_code\',\'question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_fullname\',\'question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_customer_type\')" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="account_number" class="form-control form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_account_number" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'account_number\']">\
        </div>\
		<div class="col-2 text-end dir-rtl form-group" id="doc_code_container_'+questionID+'_'+choiceID+'_'+latestCount+'">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_national_code">شماره ملی مشتری:</label>\
			<input disabled type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="national_code" class="form-control doc-national-code form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_national_code" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'national_code\']">\
			<div class="national-code-alert-box alert alert-warning align-items-center" role="alert">\
				کد وارد شده درست نیست!\
			</div>\
		</div>\
		<div class="col-3 text-end dir-rtl form-group" id="doc_code_container_'+questionID+'_'+choiceID+'_'+latestCount+'">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_fullname">نام و نام خانوادگی مشتری:</label>\
			<input disabled type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="customer_fullname" class="form-control doc-full-name form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_fullname" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'customer_fullname\']">\
		</div>\
		<div class="col-2 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_customer_type">نوع مشتری:</label>\
			<select data-element-type="doc" data-index="'+latestCount+'" data-doc-part="customer_type"  data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_customer_type" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'customer_type\']" class="form-select form-select-md text-center" aria-label="Default select example" autocomplete="off">\
				<option selected disabled>انتخاب کنید</option>\
				<option value="REAL">حقیقی</option>\
				<option value="LEGAL">حقوقی</option>\
			</select>\
		</div>\
		<div class="col-3 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_personnel_code">شماره استخدامی:</label>\
			<input onfocusout="getPersonnelByPersonnelCode(this,\'question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_personnel_code\',\'personnel_code_error_'+questionID+'_'+choiceID+'_'+latestCount+'_span\')" type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="personnel_code" class="form-control form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_description" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'personnel_code\']" >\
			<span id="personnel_code_error_'+questionID+'_'+choiceID+'_'+latestCount+'_span"></span>\
		</div>\
		<div class="col-1 text-end dir-rtl form-group hide">\
			<label>&nbsp;</label>\
			<button type="button" class="form-control btn btn-danger doc-row-btn-delete" onclick="eliminateDocRow('+questionID+','+choiceID+','+latestCount+')"><span class="close">&times;</span></button>\
		</div>\
		<div class="col-2 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_doc_number">شماره سند:</label>\
			<input type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="doc_number" class="form-control form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_description" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'doc_number\']">\
		</div>\
		<div class="col-2 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_doc_date">تاریخ سند:</label>\
			<input type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="doc_date" class="form-control form-control-sm j-date-calendar" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_description" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'doc_date\']">\
		</div>\
		<div class="col-5 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_description">توضیحات:</label>\
			<input type="text" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="description" class="form-control form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_description" name="question_filled_response['+questionID+']['+choiceID+'}]['+latestCount+'][\'description\']">\
		</div>\
        <div class="col-3 text-end dir-rtl form-group">\
          <label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_documentation_file">فایل مستندات:</label>\
          <input type="file" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="documentation_file" class="form-control documentation_file_input" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_documentation_file" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_documentation_file" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'documentation_file\']">\
		  <input class="uploaded-file-container-input" type="hidden" value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="documentation_file" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" name="question_filled_response['+questionID+']['+choiceID+']['+latestCount+'][\'documentation_file\']" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_documentation_file">\
        </div>\
		<div class="col-12 text-end dir-rtl form-group">\
			<label for="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_brboss_desc">توضیحات رئیس شعبه:</label>\
			<input type="text" disabled value="" data-element-type="doc" data-index="'+latestCount+'" data-doc-part="brboss_desc" class="form-control form-control-sm" autocomplete="off" data-questionid="'+questionID+'" data-choiceid="'+choiceID+'" id="question_response_'+questionID+'_'+choiceID+'_'+latestCount+'_brboss_desc" name="question_filled_response['+questionID+']['+choiceID+'}]['+latestCount+'][\'brboss_desc\']">\
		</div>\
      </div>';

	  
      $('#documentation_inputs_container_'+questionID+'_'+choiceID).append(rowHTML);
	  
	  $(document).on("focus", ".j-date-calendar", function() {
			if (!$(this).data("datepicker")) {
				$(this).persianDatepicker({
					formatDate: "YYYY-MM-DD",
					selectableYears: [1403, 1404],
					selectedBefore: true
				});
			}
      });
	  
	  
	  
	  $(".documentation_file_input").on('change', uploadFileFunction);

	  latestCount += 1;
      $(obj).data('latest_count',latestCount)


    }
	
	

	
	function getNationalCodeByAccountNumber(obj,nationalCodeElementID,fullnameElementID, customerTypeElementID){
		var accountNumber = $(obj).val();
		
		$.ajax({
			url: '{{route("inspection.checklist.getNationalCodeByAccountNumber")}}',
			type: 'GET',
			cache:false,
			data: {"account_number":accountNumber},
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			success: function(response, textStatus, xhr){
			  if(xhr.status==200){
				$('#'+nationalCodeElementID).val(response.data.national_code);
				$('#'+fullnameElementID).val(response.data.first_name.trim()+' '+response.data.last_name.trim());
				
				if(response.data.customer_type.trim()=="01"){
					//select REAL
					$('#'+customerTypeElementID).val('REAL');
				}else if(response.data.customer_type.trim()=="02"){
					//select LEGAL
					$('#'+customerTypeElementID).val('LEGAL');
				}else{
					$('#'+customerTypeElementID).val('');
				}
			  }
			},
			error: function(response){
			}
		});
		
		
	}
	
	
	function getPersonnelByPersonnelCode(obj,personnelCodeElementID, errorContainerID){
		var personnel_code = $(obj).val();
		
		$.ajax({
			url: '{{route("inspection.checklist.getPersonnelByPersonnelCode")}}',
			type: 'GET',
			cache:false,
			data: {"personnel_code":personnel_code},
			headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
			success: function(response, textStatus, xhr){
			  if(xhr.status==200){
				if(response.data.festno==="" || !response.data.festno){
					$('#'+errorContainerID).html("کد وارد شده اشتباه است!");
				}else{
					$('#'+errorContainerID).html(""); 
				}
			  }else{
				
				$('#'+errorContainerID).html("کد وارد شده اشتباه است!");
			  }
			},
			error: function(response){
				$('#'+errorContainerID).html("کد وارد شده اشتباه است!");
			}
		});
		
		
	}
	
	


	function printDiv(divName){
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;

	}
	
	

	
    </script>


    @yield('bottom-js-scripts')
  </body>
</html>


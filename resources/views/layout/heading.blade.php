<div id="pages_heading">
  <div class="row">
    <div class="col-6 text-end">
      بازرسی مبارزه با پولشویی {{!empty($selectedBranch)?("(شعبه ".$selectedBranch->brname.")"):''}} 
	  <span>{{!empty($persianInspectionDate)?(" - ".$persianInspectionDate):''}}</span>
    </div>
    <div class="col-6 text-start">
      <a class="btn btn-danger" href="/">ثبت بازرسی</a>
    </div>
  </div>
</div>


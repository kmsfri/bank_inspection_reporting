<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => public_path('pdf'),
	'pdf_a'                 => false,
	'pdf_a_auto'            => false,
	'icc_profile_path'      => '',
	'font_path' => public_path('/static/fonts'),
	'font_data' => [
		'vazir' => [
			'R'  => 'vazir/Vazirmatn-Regular.ttf',
			'B'  => 'vazir/Vazirmatn-Bold.ttf',
			'I'  => 'vazir/Vazirmatn-Regular.ttf',
			'BI' => 'vazir/Vazirmatn-Bold.ttf',
			'useOTL' => 0xFF,
			'useKashida' => 75,
		],
	],

];


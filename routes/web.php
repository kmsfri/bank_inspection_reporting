<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('login', [App\Http\Controllers\AuthController::class, 'index'])->name('showLoginForm');
Route::post('login', [App\Http\Controllers\AuthController::class, 'login'])->name('login'); 
Route::get('/login-from-rosha/{username}/{brcode}', [App\Http\Controllers\AuthController::class, 'loginFromRosha'])->name('loginFromRosha');
Route::get('/login-from-inspection-system/{username}/{sarparasti_code}/{user_type}/{fullname?}', [App\Http\Controllers\AuthController::class, 'loginFromInspectionSystem'])->name('loginFromInspectionSystem');

//checklist
Route::group(['middleware' => ['auth']], function() {

	Route::get('logout', [App\Http\Controllers\AuthController::class, 'logout'])->name('logout');
	
	Route::view('/', 'pages.dashboard')->name('pages.dashboard');

	Route::get('/inspection/check-list/select-branch', [App\Http\Controllers\InspectionController::class, 'showBranchSelectionPage'])->name('inspection.checklist.branchSelection');

	Route::post('/inspection/check-list/questions/start', [App\Http\Controllers\InspectionController::class, 'startChecklist'])->name('inspection.checklist.startChecklist');

	Route::get('/inspection/check-list/questions/overview/{brcode}/{inspection_date}/{rental_box?}', [App\Http\Controllers\InspectionController::class, 'showChecklistOverview'])->name('inspection.checklist.showChecklistOverView');

	Route::get('/inspection/check-list/questions/{brcode}/{inspection_date}/{category_code}/{rental_box?}', [App\Http\Controllers\InspectionController::class, 'showQuestionsPage'])->name('inspection.checklist.showQuestionPage');

	Route::post('/inspection/check-list/questions/store', [App\Http\Controllers\InspectionController::class, 'storeChecklistAnswers'])->name('inspection.checklist.storeAnswers');
	
	Route::post('/inspection/check-list/questions/previous_category', [App\Http\Controllers\InspectionController::class, 'redirectToPreviousCategory'])->name('inspection.checklist.redirectToPreviousCategory');

	Route::post('/inspection/checklist/upload/docFile', [App\Http\Controllers\InspectionController::class, 'uploadAnswerDocumentFile'])->name('inspection.checklist.uploadDocFile');
	
	Route::post('/inspection/checklist/finalize', [App\Http\Controllers\InspectionController::class, 'finalizeChecklist'])->name('inspection.checklist.finalize');
	
	Route::get('/inspection/checklist/final-page', [App\Http\Controllers\InspectionController::class, 'showFinalPage'])->name('inspection.checklist.show-final-page');
	
	Route::get('/inspection/report/chart/time-based/{based_data}/{criteria}', [App\Http\Controllers\ReportController::class, 'showTimeBasedChart'])->name('inspection.report.show-time-based-chart');
	
	Route::get('/inspection/report/list', [App\Http\Controllers\ReportController::class, 'showReportsList'])->name('inspection.report.show-reports-list');
	
	Route::get('/inspection/resources/list', [App\Http\Controllers\ReportController::class, 'showResourcesList'])->name('inspection.resources.show-resources-list');
	
	Route::post('/inspection/report/sarparasti/branches', [App\Http\Controllers\ReportController::class, 'getSarparastiBranchesReport'])->name('inspection.report.sarparasti.get-sarparasti-branches-report');
	
	Route::get('/inspection/report/{brcode}/records', [App\Http\Controllers\ReportController::class, 'showInspectionRecords'])->name('inspection.report.show-inspection-records');
	
	Route::get('/inspection/report/latest-inspections', [App\Http\Controllers\ReportController::class, 'showLatestInspections'])->name('inspection.report.show-latest-inspections');
	
	Route::get('/inspection/report', [App\Http\Controllers\ReportController::class, 'showReportPage'])->name('inspection.report.show-report-page');
	
	Route::get('/inspection/check-list/get-national-code-by-acc-number', [App\Http\Controllers\InspectionController::class, 'getNationalCodeByAccountNumber'])->name('inspection.checklist.getNationalCodeByAccountNumber'); 
	
	Route::get('/inspection/check-list/get-personnel-by-personnel_code', [App\Http\Controllers\InspectionController::class, 'getPersonnelByPersonnelCode'])->name('inspection.checklist.getPersonnelByPersonnelCode'); 
	
	
});





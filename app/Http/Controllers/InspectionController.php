<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LookupQuestion;
use App\Models\Branch;
use App\Models\Answer;
use App\Models\AnswerText;
use App\Models\AnswerDocumentation;
use App\Models\Question;
use App\Models\QuestionGroup;
use App\Models\CategoryBranchScore;
use App\Models\QuestionGroupBranchScore;
use App\Models\BranchScore;
use App\Models\UserSarparasti;
use App\Models\BranchIP;
use App\Models\ChecklistProcess;
use App\User;
use Validator;
use App\Helpers\DateHelper;
use App\Helpers\GeneralHelper;
use DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class InspectionController extends Controller
{

	public $darajehTitles = [
		0 => 'باجه',
		1 => 'یک',
		2 => 'دو',
		3 => 'سه',
		4 => 'چهار',
		5 => 'پنج',
		6 => 'نامشخص',
		7 => 'ممتاز الف',
		8 => 'ممتاز ب'
	];

    public function showBranchSelectionPage(){

	
	  if(auth()->user()->user_type=='ADMIN'){
		$sarparasties = Branch::getAllSaraparasties();
	  }else if(auth()->user()->user_type=='SARPARASTI_INSPECTOR' || auth()->user()->user_type=='BRANCH_BOSS'){
		$sarparasties = Branch::getSaraparasti(auth()->user()->sarcode);
	  }else if(auth()->user()->user_type=='GENERAL_INSPECTOR'){
		$sarparasties = UserSarparasti::where('user_id',auth()->user()->id)->get();
	  }
	  
	  $branchesList = $this->getAllBranches($sarparasties);
	  
	  $clientIP = request()->ip();
	  $clientIPToCheck = "";
	  if(!empty($clientIP)){
		$explodedIP = explode(".",$clientIP);
		$explodedIP[3] = 0;
		$clientIPToCheck = implode(".",$explodedIP);
	  }
	  
	  $selectedBranch = BranchIP::where('ip',$clientIPToCheck)->first();
	  
	  $selectedBranchCode = NULL;
	  if(!empty($selectedBranch)){
		$selectedBranchCode = $selectedBranch->brcode;
	  }
	  
	  //set rental status based on the latest inspection process
	  foreach($branchesList as $key=>$branchRow){
		$branchesList[$key]['rental']=0;
		$latestBranchScoreID = BranchScore::where('brcode', $branchRow['brcode'])->max('id');
		if(!empty($latestBranchScoreID)){
			$latestBranchScore = BranchScore::find($latestBranchScoreID);
			$branchesList[$key]['rental'] = $latestBranchScore->has_rental_box;
			
		}
	  }
	  
	  
      return view('pages.branch-selection',['branchesList' => $branchesList, 'selectedBranchCode'=>$selectedBranchCode]);

    }
	
	
	
	private function getAllBranches($sarparasties){
	
		$branchesList = [];
		foreach($sarparasties as $sarparastiItem){
			$branches = Branch::select('brcode', 'brname', 'arzi', 'sarcode')
								->where('sarcode',$sarparastiItem->sarcode)
								->where('sarcode',"<>",290);
			
			if(auth()->user()->user_type=='BRANCH_BOSS'){
				$branches = $branches->where('brcode',auth()->user()->brcode);
			}
			
			$branchesList = array_merge($branchesList, $branches->get()->toArray());
		}
						
		return $branchesList;
		
	}


    public function startChecklist(Request $request){

      $v = Validator::make($request->all(), [
          'brcode' => 'required|integer',
          'inspection_date' => 'required|max:10',
		  'rental_box' => 'nullable|integer',
      ]);

      if ($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }
	  
	  $gregorianStartData = DateHelper::jalaliToGregorianString($request->inspection_date,"-");
	  
	  $activeChecklistProcess = $this->createProcessIfNotExists($gregorianStartData, $request->brcode);
	  
	  $branchScore = NULL;
	  if(!empty($activeChecklistProcess) && !empty($activeChecklistProcess['activeChecklistProcess'])){
		$branchScore = BranchScore::where('CHECKLIST_PROCESS_ID', $activeChecklistProcess['activeChecklistProcess']->id)->first();
	  }
	  
	  if(empty($branchScore) && !empty($activeChecklistProcess) && !empty($activeChecklistProcess['activeChecklistProcess'])){
	  
		$branch = Branch::where('brcode',$request->brcode)->first();
		
		
		$inspectionDateExploded = explode('-',$request->inspection_date);
		$period = $inspectionDateExploded[0];
		
	  
		$branchScore = new BranchScore();

		$branchScore->brcode = $request->brcode;
		$branchScore->period = $period;
		$branchScore->CHECKLIST_PROCESS_ID = $activeChecklistProcess['activeChecklistProcess']->id;
		$branchScore->score = 0;
		$branchScore->has_rental_box = ($request->rental_box??0);
		$branchScore->arzi_branch = $branch->arzi;

		$branchScore->save();
	  }
	  
	  
	  

      $firstCategory = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('IS_ACTIVE',1)
                        ->orderBy('RANK','ASC')
                        ->firstOrFail();

      $checkListUrl = route('inspection.checklist.showQuestionPage',[
                        'brcode'=>$request->brcode,
                        'inspection_date'=>urlencode($request->inspection_date),
                        'category_code'=>$firstCategory->CODE,
						'rental_box'=>$request->rental_box,
                      ]);


      return redirect($checkListUrl);


    }

	
	private function createProcessIfNotExists($gregorianStartDate, $brcode){
	  
		$errorMessage = NULL;
	  
		$openedChecklistProcess = $this->getOpenBranchInspectionProcess($brcode, $gregorianStartDate);
		$activeChecklistProcess = $this->getActiveBranchInspectionProcess($brcode, $gregorianStartDate);

		$errorMessage = "";

		if(!empty($openedChecklistProcess) && empty($activeChecklistProcess)){

			$errorMessage = "برای شعبه چک لیست با تاریخ شروع: ".$openedChecklistProcess->started_at." باز شده است. لطفا تاریخ چک لیست را بعد از تاریخ شروع قرار دهید.";

		}else if(empty($openedChecklistProcess) && empty($activeChecklistProcess)){
		//create a new checklist process

			$startedAtJalali = DateHelper::gregorianToJalaliString($gregorianStartDate);
		
			$process = New ChecklistProcess();
			$process->brcode = $brcode;
			$process->started_at = $gregorianStartDate;
			$process->started_at_jalali = $startedAtJalali;
			$process->ended_at = NULL;

			$process->save();
		}

		$activeChecklistProcess = $this->getActiveBranchInspectionProcess($brcode, $gregorianStartDate);
		
		
		return [
				'errorMessage' => $errorMessage,
				'activeChecklistProcess' => $activeChecklistProcess,
		];
	  }
	
	
	private function getActiveBranchInspectionProcess($brcode, $gregorianStartDate){
		$checklistProcess = ChecklistProcess::where('brcode',$brcode)
								->where(function($query) use($gregorianStartDate) {
									$query->where(function($query) use($gregorianStartDate) {
										$query->whereNotNull('started_at')
												->whereNull('ended_at')
												->where('started_at','<=',$gregorianStartDate);
									})->orWhere(function($query) use($gregorianStartDate) {
										$query->whereNotNull('started_at')
												->whereNotNull('ended_at')
												->where('started_at','<=',$gregorianStartDate)
												->where('ended_at','>=',$gregorianStartDate);
									});
								})
								->first();
								
								
		return $checklistProcess;
	}
	
	
	private function getOpenBranchInspectionProcess($brcode, $gregorianStartDate){
		$checklistProcess = ChecklistProcess::where('brcode',$brcode)
								->where(function($query) use($gregorianStartDate) {
									$query->whereNotNull('started_at')
											->whereNull('ended_at');
								})
								->first();
								
		return $checklistProcess;
	}

    public function showQuestionsPage(Request $request){

      $inputData = [
        'brcode' => $request->brcode,
        'inspection_date' => $request->inspection_date,
        'category_code' => $request->category_code,
      ];

      $v = Validator::make($inputData, [
          'brcode' => 'required|integer',
          'inspection_date' => 'required|max:10',
          'category_code' => 'nullable|max:30',
      ]);

      if ($v->fails())
      {
        return redirect(route('inspection.checklist.branchSelection'))->withErrors($v->errors());
      }



      $allCategories = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('IS_ACTIVE',1);
						
						
	  if(empty($request->rental_box) || $request->rental_box==0 || $request->rental_box==""){
		$allCategories = $allCategories->whereNotIn('CODE',['RENTAL_FUND']);
	  }
						
      $allCategories = $allCategories->orderBy('RANK','ASC')
                        ->get();


      $selectedLookup = LookupQuestion::select('*')
                            ->where('CODE',$request->category_code)->firstOrFail();

      $categoryQuestions = $selectedLookup->questions()
                                          ->whereNull('FK_QUESTION')
                                          ->orderBy('QUESTION_NO','ASC')
                                          ->with('subQuestions')
                                          ->get();
										  
										  
	  $categoryQuestionIDs = $selectedLookup->questions()
                                          ->whereNull('FK_QUESTION')
                                          ->orderBy('QUESTION_NO','ASC')
                                          ->with('subQuestions')
                                          ->get()->pluck('ID')->toArray();

		

	  
	  $gregorianStartData = DateHelper::jalaliToGregorianString($request->inspection_date,"-");
	  $activeChecklistProcess = $this->getActiveBranchInspectionProcess($request->brcode, $gregorianStartData);
	  
	  $disabledFields = false;
	  if(!empty($activeChecklistProcess->ended_at)){
		$disabledFields = true;
	  }
	  
	  
      $questionChoices = [];
      foreach($categoryQuestions as $questionItem){

	  
			
	  
          $questionItemChoicePatterns = $questionItem->answerPatterns()->orderBy('PATTERN_ORDER','ASC')->get();			

          //get each pattern answers
          foreach($questionItemChoicePatterns as $choicePattern){

		  
            $questionChoices[$questionItem->ID][] =
              [
                $choicePattern->ID => $choicePattern->answers()->get()
              ];
			
          }
		  
		  
		  


          foreach($questionItem->subQuestions()->get() as $nestedQuestionItem){

              $nestedQuestionItemChoicePatterns = $nestedQuestionItem->answerPatterns()->get();
              foreach($nestedQuestionItemChoicePatterns as $nestedChoicePattern){

                $questionChoices[$nestedQuestionItem->ID][] =
                  [
                    'choices' => $nestedChoicePattern->answers()->get()
                  ];
              }
			  

          }

      }
	  
	  
	  
	  
	  
	  
	  //get previous answer in this period
	  $inspectionDateExploded = explode('-',$request->inspection_date);
	  
	  $period = $inspectionDateExploded[0];
	  $brcode = $request->brcode;
	  
	  
	  $filledAnswers = Answer::where('period',$period)
						->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
                        ->where('FK_BRC',$brcode)->get();
						
	  				
	  $choicesData = [];
	  
	  foreach($filledAnswers as $choiceDataItem){
		$choicesData[$choiceDataItem->FK_QUE][$choiceDataItem->FK_PTNANS] = $choiceDataItem->SELECTED_ANSWER;
	  }
						
						
	  $filledAnswersIDs = Answer::where('period',$period)
						->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
                        ->where('FK_BRC',$brcode)->get()->pluck('ID')->toArray();
						

	
	  $filledTextareas = AnswerText::whereIn('FK_ANS', $filledAnswersIDs)->get();
	  $textsData = [];
	  foreach($filledTextareas as $textDataItem){
		$relatedAnswer = Answer::where('ID',$textDataItem->FK_ANS)->first();
		$textsData[$relatedAnswer->FK_QUE][$textDataItem->FK_ANS_ITEM_ID] = $textDataItem->ANSWER_TEXT;
	  }
	  
	  
	  $filledDocuments = AnswerDocumentation::whereIn('FK_ANS', $filledAnswersIDs)->get();
	  
	  $documentsData = [];
	  foreach($filledDocuments as $DocDataItem){
		$relatedAnswer = Answer::where('ID',$DocDataItem->FK_ANS)->first();
		$documentsData[$relatedAnswer->FK_QUE][$DocDataItem->FK_ANS_ITEM_ID][] = $DocDataItem->DOCUMENTATION;
	  }
	  
	  
	  
	  $questionGroupUnderTheSelectedCategory = QuestionGroup::where('category_key',$request->category_code)->get();
	  

	  $selectedBranch = Branch::where('brcode',$request->brcode)->first();
	  
	  
	  $choiceButtonTypes = [
			13 => 'fully-observed',
			14 => 'not-observed-main',
			15 => 'partially-observed',
			16 => 'not-observed',
		];
		
		
	  $categoryFilledAnswers = [];
	  foreach($filledAnswers as $filledItem){
		if( in_array($filledItem->FK_QUE, $categoryQuestionIDs )){
			$categoryFilledAnswers[] = $filledItem;
		}
	  }	  	
	  
	  $isAllCategoryQuestionsAnswered = ( count($categoryFilledAnswers) == count($categoryQuestions) );
	  
	  $persianInspectionDate = strtr($request->inspection_date, array('0'=>'۰','1'=>'۱','2'=>'۲','3'=>'۳','4'=>'۴','5'=>'۵','6'=>'۶','7'=>'۷','8'=>'۸','9'=>'۹'));
	  $persianInspectionDate = str_replace('-','/',$persianInspectionDate);
	  
	  
	  $disabledBrBossFields = false;
      if(auth()->user()->user_type!=="BRANCH_BOSS"){
		$disabledBrBossFields = true;
	  }
	  
	  
	 
      return view('pages.checklist-questions',[
        'selectedBrCode' => $request->brcode,
        'inspectionDate' => $request->inspection_date,
		'persianInspectionDate' => $persianInspectionDate,
        'categoryCode' => $request->category_code,
        'activeCategoryCode' => $request->category_code,
		'rentalBox' => $request->rental_box,
        'allCategories' => $allCategories,
        'categoryQuestions' => $categoryQuestions,
        'questionChoices' => $questionChoices,
		
		'choicesData' => $choicesData,
		'textsData' => $textsData,
		'documentsData' => $documentsData,
		
		'questionGroupUnderTheSelectedCategory' => $questionGroupUnderTheSelectedCategory,
		'userType' => auth()->user()->user_type,
		'selectedBranch' => $selectedBranch,
		
		'buttonTypes' => $choiceButtonTypes,
		'isAllCategoryQuestionsAnswered' => $isAllCategoryQuestionsAnswered,
		
		'rental_box' => ($request->rental_box??0),
		'disabledFields' => $disabledFields,
		'disabledBrBossFields' => $disabledBrBossFields,
		
      ]);

    }

	

    public function storeChecklistAnswers(Request $request){




      $v = Validator::make($request->all(), [
          'brcode' => 'required|integer',
          'inspectionDate' => 'required|max:10',
          'categoryCode' => 'required|max:30',
          'selectedChoices' => 'required|array',
          'filledTextareas' => 'nullable|array',
          'filledDocuments' => 'nullable|array',
      ]);

      if ($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }
	  
	  
	  $inspectionDateExploded = explode('-',$request->inspectionDate);
	  $period = $inspectionDateExploded[0];

	  
	  
	  $gregorianStartData = DateHelper::jalaliToGregorianString($request->inspectionDate,"-");
	  $activeChecklistProcess = $this->getActiveBranchInspectionProcess($request->brcode, $gregorianStartData);


      //TODO: Store in db
	  $filledDocuments = [];


	  foreach( ($request->filledDocuments??[]) as $questionID=>$questionDocItems){
		if(empty($questionDocItems) || count($questionDocItems)<=1){
			continue;
		}

		
		foreach($questionDocItems as $docItem){			
			$filledDocuments[$questionID][$docItem[0]][$docItem[1]][$docItem[2][0]] =  $docItem[2][1];
		}
		
	  }
	  

	  
	  
	  $filledTextareas = [];
	  foreach( ($request->filledTextareas??[]) as $questionTextItems){
		if(empty($questionTextItems)){
			continue;
		}
		
		foreach($questionTextItems as $textItem){
			$filledTextareas[$textItem[0]][$textItem[1]] =  $textItem[2];
		}
		
	  }

	  $AnswerChoicesDataArray = [];
	  
	  $sumOfScores = 0;
	  $countOfScores = 0;
	  
	  $sumOfQuestionGroupBranchScores = [];
	  $countOfQuestionGroupBranchScores = [];
	
	
	  if(empty($activeChecklistProcess->ended_at) && auth()->user()->user_type!='BRANCH_BOSS'){
	  
		foreach($request->selectedChoices as $questionID=>$choiceItemID){
							
				
			if(empty($choiceItemID)){
				continue;
			}

			$question = Question::where('ID',$questionID)->first();

			$previousAnswerIDs = Answer::where('FK_CHKLST', 1)
									->where('FK_BRC', $request->brcode)
									->where('FK_QUE', $questionID)
									->where('PERIOD', $period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->get()->pluck('ID')->toArray();
									
							
			//remove previous registered answers
			Answer::where('FK_CHKLST', 1)
									->where('FK_BRC', $request->brcode)
									->where('FK_QUE', $questionID)
									->where('PERIOD', $period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->delete();

			$currentQuestionGroup = QuestionGroup::where('id',$question->FK_QUESTION_GROUP)->first();
			$questionGroupFactor = $currentQuestionGroup->factor;
									
									


			$score = NULL;
			
			if($currentQuestionGroup->category_key=='GENERAL_CHECK'){
				$score = 0;
				if($choiceItemID==2){ //YES
					$score = 1;
				}
			}else{
				if($filledTextareas[$questionID][20]==0){
					$score = 0;
				}else{
					$value1 = $filledTextareas[$questionID][21];
					$value2 = $filledTextareas[$questionID][20];
					if($value1>$value2){
						$value1 = $value2;
					}
					
					$filledTextareas[$questionID][21] = $value1;
					$score = round(($value1/$value2)*$questionGroupFactor,2);
				}
				
			}
			
			if($choiceItemID != 13){
				$sumOfScores += $score??0;
				$countOfScores++;
			}

			$answer = New Answer();
			$answer->FK_CHKLST = 1;
			$answer->FK_QUE = $questionID;
			$answer->FK_PTNANS = $choiceItemID;
			$answer->FK_BRC = $request->brcode;
			$answer->PERIOD = $period;
			$answer->CHECKLIST_PROCESS_ID = $activeChecklistProcess->id;
			$answer->ANSWER_POINT = $score;
			$answer->SELECTED_ANSWER = $choiceItemID;
			$answer->RESPONDER_USER = auth()->user()->id;
			

			$answer->save();

			$answerID=$answer->id;

			$sumOfQuestionGroupBranchScores[$question->FK_QUESTION_GROUP] = ($sumOfQuestionGroupBranchScores[$question->FK_QUESTION_GROUP]??0)+$score;
			$countOfQuestionGroupBranchScores[$question->FK_QUESTION_GROUP] = ($countOfQuestionGroupBranchScores[$question->FK_QUESTION_GROUP]??0)+1;



			//remove previous filled textareas
			AnswerText::whereIn('FK_ANS', $previousAnswerIDs)->delete();
			
			if(!empty($filledTextareas[$questionID])){
				foreach($filledTextareas[$questionID] as $choiceItemID=>$filledTextItem){
				
					$answerText = New AnswerText();
					
					$answerText->FK_ANS = $answerID;
					$answerText->FK_ANS_ITEM_ID = $choiceItemID;
					$answerText->ANSWER_TEXT = $filledTextItem;
					
					$answerText->save();
				}
			}

			//remove previous answered documents
			AnswerDocumentation::whereIn('FK_ANS', $previousAnswerIDs)->delete();

			if(!empty($filledDocuments[$questionID])){
				foreach($filledDocuments[$questionID] as $choiceItemID=>$filledDocArray){
					
					foreach($filledDocArray as $filledDocItem){
						$answerDocument = New AnswerDocumentation();
						
						$answerDocument->FK_ANS = $answerID;
						$answerDocument->FK_ANS_ITEM_ID = $choiceItemID;
						$answerDocument->DOCUMENTATION = json_encode($filledDocItem, true);
						
						$answerDocument->save();
					}
					
				}
			}

		}
		
		foreach($sumOfQuestionGroupBranchScores as $groupID=>$groupSumScoreItem){
			
		
			$currentQuestionGroup = QuestionGroup::where('id',$groupID)->first();
			$questionGroupFactor = $currentQuestionGroup->factor;
		
			$scoreG = $countOfQuestionGroupBranchScores[$groupID]*$questionGroupFactor;
		
			if($currentQuestionGroup->category_key=='GENERAL_CHECK'){
				
				
				$scoreH = round(($groupSumScoreItem/$scoreG)*100);				
				
			}else{
				
				$scoreH = round((100-(($groupSumScoreItem/$scoreG)*100)));
				
			}
			
			//remove previous group score
			QuestionGroupBranchScore::where('question_group_id', $groupID)
									->where('category_key', $request->categoryCode)
									->where('brcode', $request->brcode)
									->where('period', $period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->delete();

			//add new question group score
			$groupQuestionScore = new QuestionGroupBranchScore();

			$groupQuestionScore->question_group_id = $groupID;
			$groupQuestionScore->category_key = $request->categoryCode;
			$groupQuestionScore->brcode = $request->brcode;
			$groupQuestionScore->period = $period;
			$groupQuestionScore->CHECKLIST_PROCESS_ID = $activeChecklistProcess->id;
			$groupQuestionScore->score = $scoreH;

			$groupQuestionScore->save();


		}

		$averageOfCategoryScores = 0;
		if($countOfScores>0){
			if($request->categoryCode=="GENERAL_CHECK"){
				$averageOfCategoryScores = round((($sumOfScores/$countOfScores)*100),2);
			}else{
				$averageOfCategoryScores = round(($sumOfScores/$countOfScores),2);
			}
			
		}
		
		if($request->categoryCode!="GENERAL_CHECK"){
			$averageOfCategoryScores = 100-$averageOfCategoryScores;
		}
		

		//remove previous value
		CategoryBranchScore::where('category_key', $request->categoryCode)
								->where('brcode', $request->brcode)
								->where('period', $period)
								->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
								->delete();
								
		

		//add new value
		$categoryScore = new CategoryBranchScore();

		$categoryScore->category_key = $request->categoryCode;
		$categoryScore->brcode = $request->brcode;
		$categoryScore->period = $period;
		$categoryScore->CHECKLIST_PROCESS_ID = $activeChecklistProcess->id;
		$categoryScore->score = $averageOfCategoryScores;

		$categoryScore->save();
	  }else if(auth()->user()->user_type=='BRANCH_BOSS'){ //responder is branch boss
		foreach($request->selectedChoices as $questionID=>$choiceItemID){
			if(empty($choiceItemID)){
				continue;
			}
			
			
			$previousAnswerIDs = Answer::where('FK_CHKLST', 1)
									->where('FK_BRC', $request->brcode)
									->where('FK_QUE', $questionID)
									->where('PERIOD', $period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->get()->pluck('ID')->toArray();
									
			$answer = Answer::where('FK_CHKLST', 1)
									->where('FK_BRC', $request->brcode)
									->where('FK_QUE', $questionID)
									->where('PERIOD', $period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->firstOrFail();				
						
									
			//remove previous answered documents
			AnswerDocumentation::whereIn('FK_ANS', $previousAnswerIDs)->delete();

			if(!empty($filledDocuments[$questionID])){
				foreach($filledDocuments[$questionID] as $choiceItemID=>$filledDocArray){
					
					foreach($filledDocArray as $filledDocItem){
						$answerDocument = New AnswerDocumentation();
						
						$answerDocument->FK_ANS = $answer->ID;
						$answerDocument->FK_ANS_ITEM_ID = $choiceItemID;
						$answerDocument->DOCUMENTATION = json_encode($filledDocItem, true);
						
						$answerDocument->save();
					}
					
				}
			}						
									
										
			
			
			if(!empty($filledTextareas[$questionID])){
				$branchBossResponse = NULL;
				foreach($filledTextareas[$questionID] as $choiceItemID=>$filledTextItem){
				
					if($choiceItemID==18){
						
					
						$branchBossResponse = $filledTextItem;
					
						$answerText = New AnswerText();
							
						$answerText->FK_ANS = $answer->ID;
						$answerText->FK_ANS_ITEM_ID = $choiceItemID;
						$answerText->ANSWER_TEXT = $filledTextItem;
						
						$answerText->save();
					}		
				}
				
				if(!empty($branchBossResponse)){
					
									
					$answer->STEATMENT_ANSWER_BRANCH = $branchBossResponse;
					$answer->save();
				}
				
			}
			
			
			
			
		}
	  }	  
	  
	  

      //GET Next Category
      $currentCategory = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('CODE',$request->categoryCode)
                        ->where('IS_ACTIVE',1)
                        ->firstOrFail();

      $nextCategory = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('RANK','>',$currentCategory->RANK)
                        ->where('IS_ACTIVE',1);
	
	
	  if(empty($request->rental_box) || $request->rental_box==0 || $request->rental_box==""){
		$nextCategory = $nextCategory->whereNotIn('CODE',['RENTAL_FUND']);
	  }
	
	  $nextCategory = $nextCategory
                        ->orderBy('RANK','ASC')
                        ->first();



      $nextUrl = Null;
      if(!empty($nextCategory)){
        $nextUrl = route('inspection.checklist.showQuestionPage',[
                            'brcode'=>$request->brcode,
                            'inspection_date'=>$request->inspectionDate,
                            'category_code'=>$nextCategory->CODE,
							'rental_box' => ($request->rental_box??0)
                        ]);
      }else{
	  
		
		if(empty($activeChecklistProcess->ended_at) && auth()->user()->user_type!='BRANCH_BOSS'){
			//remove previous checklist score for branch in a period
			BranchScore::where('brcode', $request->brcode)
						->where('period', $period)
						->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
						->delete();
			
			//calculate and add new checklist score for branch in a period
			
			$excludeCategories = [];
			if(empty($request->rental_box) || $request->rental_box == 0){
				$excludeCategories = ['RENTAL_FUND'];
			}else{
				$excludeCategories = [''];
			}
			
			$groups = QuestionGroup::whereNotIn('category_key',$excludeCategories)->get();
			
			
			$branchScoreValue = 0;
			foreach($groups as $groupItem){
				$branchGroupScoreObj = QuestionGroupBranchScore::where('question_group_id',$groupItem->id)
															->where('brcode',$request->brcode)
															->where('period',$period)
															->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
															->first();
															
				$branchGroupScore = !empty($branchGroupScoreObj)?$branchGroupScoreObj->score:0;
				
				$branchScoreValue += $branchGroupScore;
									
			}
			
			$branchScoreValue = round($branchScoreValue/count($groups));
			
			

			
			
			Branch::where('brcode',$request->brcode)->update(['has_rental_box'=>($request->rental_box??0)]);
			
			$branch = Branch::where('brcode',$request->brcode)->first();
			
			
			$branchScore = new BranchScore();
		  
			$branchScore->brcode = $request->brcode;
			$branchScore->period = $period;
			$branchScore->CHECKLIST_PROCESS_ID = $activeChecklistProcess->id;
			$branchScore->score = $branchScoreValue;
			$branchScore->has_rental_box = ($request->rental_box??0);
			$branchScore->arzi_branch = $branch->arzi;
			
			$branchScore->save();
			
		}
		
        $nextUrl = route('inspection.checklist.showChecklistOverView',[
                            'brcode'=>$request->brcode,
                            'inspection_date'=>$request->inspectionDate,
							'rental_box'=>($request->rental_box??0),
							'store_step' => 1
                        ]);
      }


      $result = [
        'response_code' => 200,
        'next_url' => $nextUrl,
      ];


      return $result;



    }
	
	
	
	
	public function redirectToPreviousCategory(Request $request){




      $v = Validator::make($request->all(), [
          'brcode' => 'required|integer',
          'inspectionDate' => 'required|max:10',
          'categoryCode' => 'required|max:30'
      ]);

      if ($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }
	  
	  
	  $inspectionDateExploded = explode('-',$request->inspectionDate);
	  $period = $inspectionDateExploded[0];


      
	  
	  

      //GET Next Category
      $currentCategory = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('CODE',$request->categoryCode)
                        ->where('IS_ACTIVE',1)
                        ->firstOrFail();

      $nextCategory = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('RANK','<',$currentCategory->RANK)
                        ->where('IS_ACTIVE',1);
	
	
	  if(empty($request->rental_box) || $request->rental_box==0 || $request->rental_box==""){
		$nextCategory = $nextCategory->whereNotIn('CODE',['RENTAL_FUND']);
	  }
	
	  $nextCategory = $nextCategory
                        ->orderBy('RANK','DESC')
                        ->first();



      $nextUrl = Null;
      if(!empty($nextCategory)){
        $nextUrl = route('inspection.checklist.showQuestionPage',[
                            'brcode'=>$request->brcode,
                            'inspection_date'=>$request->inspectionDate,
                            'category_code'=>$nextCategory->CODE,
							'rental_box' => ($request->rental_box??0)
                        ]);
      }else{
	  
		
		$nextUrl = route('pages.dashboard');
        
      }


      $result = [
        'response_code' => 200,
        'next_url' => $nextUrl,
      ];


      return $result;



    }


    public function showChecklistOverview(Request $request){


      $inputData = [
        'brcode' => $request->brcode,
        'inspection_date' => $request->inspection_date,
		'store_step' => $request->store_step
      ];

      $v = Validator::make($inputData, [
          'brcode' => 'required|integer',
          'inspection_date' => 'required|max:10',
		  'store_step' => 'required|integer'
      ]);

      if ($v->fails())
      {
        return redirect(route('inspection.checklist.branchSelection'))->withErrors($v->errors());
      }

	  $gregorianStartData = DateHelper::jalaliToGregorianString($request->inspection_date,"-");
	  $activeChecklistProcess = $this->getActiveBranchInspectionProcess($request->brcode, $gregorianStartData);
	  
	  $disabledFields = false;
	  if(!empty($activeChecklistProcess->ended_at)){
		$disabledFields = true;
	  }
	  

      $allCategories = LookupQuestion::where('FK_LKT','QUESTION_GROUP')
                        ->where('IS_ACTIVE',1)
                        ->orderBy('RANK','ASC')
                        ->get();


	  $branch = Branch::where('brcode',$request->brcode)->first();
      $selectedBrName = $branch->brname;
	  
	  $inspectionDateExploded = explode('-',$request->inspection_date);
	  $period = $inspectionDateExploded[0];
	  
	  $scoreStatusArray = $this->getInspectionScoreDetails($period, $request->brcode, $request->inspection_date);
	  
	  $previousPeriod = $period-1;
	  $previousPeriodScoreStatusArray = $this->getInspectionScoreDetails($previousPeriod, $request->brcode, $request->inspection_date);

	  $inspectorsIDs = Answer::where('period',$period)
							->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
							->where('FK_BRC',$request->brcode)
							->groupBy('RESPONDER_USER')
							->get()
							->pluck('RESPONDER_USER')
							->toArray();
							
	  $inspectors = User::whereIn('id',$inspectorsIDs)
							->get()
							->pluck('fullname')
							->toArray();
							
	  if(!empty($activeChecklistProcess->branch_boss_id)){
		$branchBoss = User::find($activeChecklistProcess->branch_boss_id);
	  }else{
		$branchBoss = User::where('brcode',$request->brcode)
							->first();
	  }
	  
	  $branchBossFullName = !empty($branchBoss)?$branchBoss->fullname:'-';
	  
	  
	  $branchScore = BranchScore::where('brcode',$request->brcode)
								->where('period',$period)
								->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
								->first();
	  
	  $inspectorResponse = $activeChecklistProcess->inspector_description;
	  
	  $explodedDate = explode(" ",$activeChecklistProcess->started_at);
	  $inspectionStartDate="-";
	  if(!empty($explodedDate[0])){
		$inspectionStartDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
	  }
	  
	  $explodedDate = explode(" ",$activeChecklistProcess->ended_at);
	  $inspectionEndDate = "-";
	  if(!empty($explodedDate[0])){
		$inspectionEndDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
	  }
	  
	  
	  $persianInspectionDate = strtr($period, array('0'=>'۰','1'=>'۱','2'=>'۲','3'=>'۳','4'=>'۴','5'=>'۵','6'=>'۶','7'=>'۷','8'=>'۸','9'=>'۹'));
	  $persianInspectionDate = str_replace('-','/',$persianInspectionDate);

	  
	  $rentalBox = $request->rental_box??0;
		
		

	  $selectedBranchStatistics = GeneralHelper::getBranchesStatisticsByAllStatuses(Branch::where('brcode',$request->brcode)->get()->pluck('brcode')->toArray());

	  $totalRequiredQuestionsCount = Question::select();
	  if($branch->arzi==0){
		  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('ARZI_QUESTION',0);
	  }

	  if($rentalBox==0){
		  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
	  }

	  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->count();


	  $respondAllRequiredQuestions = true;
	  $completedChecklistMessage = NULL;
	  if($selectedBranchStatistics[$request->brcode]>=0 && $selectedBranchStatistics[$request->brcode]<$totalRequiredQuestionsCount){
		  $respondAllRequiredQuestions = false;
		  $completedChecklistMessage = 'شما از تعداد '.$totalRequiredQuestionsCount." سوال مورد نیاز، به تعداد ".$selectedBranchStatistics[$request->brcode]." سوال پاسخ داده اید. برای ثبت نهایی چک لیست لازم است به تمامی سوالات پاسخ دهید.";
	  }
	  
	  
	  $branchExtraInfo = \App\Models\BranchExtraInfo::where('brcode',$request->brcode)->first();
	  
	  $branchclassification = \App\Models\BranchClassification::where('brcode',$request->brcode)->first();
		
		
	  $darajehTitle = $branchclassification->darajeh;
	  if(!empty($this->darajehTitles[$branchclassification->darajeh])){
		$darajehTitle = $this->darajehTitles[$branchclassification->darajeh];
	  }
	  
	  if($request->store_step==2){
		$disabledFields = true;
	  }
	  
      return view('pages.checklist-overview',[
        'selectedBrCode' => $request->brcode,
        'selectedBrName' => $selectedBrName,
        'inspectionDate' => $request->inspection_date,
		'period' => $period,
        'allCategories' => $allCategories,
		'scoreStatusArray' => $scoreStatusArray,
		'previousPeriodScoreStatusArray' => $previousPeriodScoreStatusArray,
		'inspectors' => $inspectors,
		'branchBoss' => $branchBossFullName,
		'inspectorResponse' => $inspectorResponse,
		'user_type' => auth()->user()->user_type,
		'inspectionStartDate' => $inspectionStartDate,
		'inspectionEndDate' => $inspectionEndDate,
		'selectedBranch' => $branch,
		'persianInspectionDate' => $persianInspectionDate,
		'rentalBox' => $rentalBox,
		'respondAllRequiredQuestions' => $respondAllRequiredQuestions,
		'completedChecklistMessage' => $completedChecklistMessage,
		'disabledFields'=> $disabledFields,
		'branchExtraInfo' => $branchExtraInfo,
		'branchclassification' => $branchclassification,
		'darajehTitle' => $darajehTitle,
		'storeStep' => $request->store_step,
		'inspectorProgramNumber' => $activeChecklistProcess->inspector_program_number,
		'inspectorProgramDate' => $activeChecklistProcess->inspector_program_date,
      ]);


    }
	
	
	private function getInspectionScoreDetails($period, $brcode, $inspectionDate){
	
	
		$gregorianStartData = DateHelper::jalaliToGregorianString($inspectionDate,"-");
	    $activeChecklistProcess = $this->getActiveBranchInspectionProcess($brcode, $gregorianStartData);
	
		$branchScore = BranchScore::where('brcode',$brcode)
									->where('period',$period)
									->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
									->first();
			
		$score = $branchScore->score??NULL;
		
		$statusTitle = "-";
		if($score<=30){
			$statusTitle = "خیلی ضعیف";
			$styleClass = "badge bg-danger";
		}else if($score>30 && $score<=50){
			$statusTitle = "ضعیف";
			$styleClass = "badge bg-warning bg-orange";
		}else if($score>50 && $score<=70){
			$statusTitle = "متوسط";
			$styleClass = "badge bg-warning";
		}else if($score>70 && $score<=100){
			$statusTitle = "خوب";
			$styleClass = "badge bg-success";
		}

		
		$inspected = Answer::where('period',$period)
							->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
							->where('FK_BRC',$brcode)
							->exists();
								
		if(!$inspected){
			$scoreStatusArray = [
				'avg' => 'بازرسی نشده',
				'title' => 'بازرسی نشده',
				'style_class' => "badge bg-info"
			];
		}else{
			$scoreStatusArray = [
				'avg' => $score,
				'title' => $statusTitle,
				'style_class' => $styleClass
			];
		}
		
		return $scoreStatusArray;
	}
	
	
	
	
	
	
	
	public function uploadAnswerDocumentFile(Request $request){
		

		$response = [
			'status' => 0,
			'docFile' => '',
			'message'=>'',
		];
		if ( 0 < $_FILES['file']['error'] ) {
			$response = [
				'status' => 500,
				'docFile' => '',
				'message'=>$_FILES['file']['error'],
			];
		}
		else {
		
			$toUploadPath = 'docFiles/' . time().$_FILES['file']['name'];
			$docFileDir = public_path($toUploadPath);
			move_uploaded_file($_FILES['file']['tmp_name'], $docFileDir);
			
			$response = [
				'status' => 200,
				'docFile' => $toUploadPath,
				'message'=>'SUCCESS',
			];
			
		}

		return response()->json($response, 200);

	}
	
	
	
	public function finalizeChecklist(Request $request){
	  $v = Validator::make($request->all(), [
          'brcode' => 'required|integer',
          'period' => 'required|integer',
		  'inspector_description' => 'nullable',
		  'inspector_program_number' => 'nullable|integer',
		  'inspector_program_date' => 'nullable|max:10',
		  'inspection_date' => 'required|max:10',
		  'branch_boss_personnel_code' => 'nullable|integer|exists:users,username',
		  'store_step' => 'required|integer'
      ]);

	  
      if ($v->fails())
      {
          return redirect()->back()->withErrors($v->errors());
      }
	  
	  
	  
	  $selectedBranchStatistics = GeneralHelper::getBranchesStatisticsByAllStatuses(Branch::where('brcode',$request->brcode)->get()->pluck('brcode')->toArray());

	  
	  
	  $branch = Branch::where('brcode',$request->brcode)->first();
	  
	  $totalRequiredQuestionsCount = Question::select();
	  if($branch->arzi==0){
		  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('ARZI_QUESTION',0);
	  }
	  
	  $gregorianStartData = DateHelper::jalaliToGregorianString($request->inspection_date,"-");
	  $activeChecklistProcess = $this->getActiveBranchInspectionProcess($request->brcode, $gregorianStartData);

	  
	  $branchScore = BranchScore::where('brcode',$request->brcode)
										->where('period',$request->period)
										->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
										->first();
										
	  $hasRentalBox = 0;
	  if(isset($branchScore->has_rental_box)){
		$hasRentalBox = $branchScore->has_rental_box;
	  }else{
		$hasRentalBox = $branch->has_rental_box;
	  }
	  
	  if($hasRentalBox==0){
		  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
	  }

	  $totalRequiredQuestionsCount = $totalRequiredQuestionsCount->count();
	  
	  
	  
	  
	  $disabledFields = false;
	  if(!empty($activeChecklistProcess->ended_at)){
		$disabledFields = true;
	  }
	  
	  $respondAllRequiredQuestions = true;
	  if($selectedBranchStatistics[$request->brcode]>=0 && $selectedBranchStatistics[$request->brcode]<$totalRequiredQuestionsCount){
		$respondAllRequiredQuestions = false;
	  }
	  
	  
	  $completedChecklistMessage = NULL;
	  
	  if((auth()->user()->user_type=="SARPARASTI_INSPECTOR" || auth()->user()->user_type=="GENERAL_INSPECTOR") && !$disabledFields){
	  
		  $branchScore = BranchScore::where('brcode',$request->brcode)
										->where('period',$request->period)
										->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
										->first();
										
		  if($request->store_step==1){

			$branchBoss = Null;
			if(!empty($request->branch_boss_personnel_code)){
				$branchBoss = User::where('username',$request->branch_boss_personnel_code)->first();
			}elseif(!empty($activeChecklistProcess->branch_boss_id)){
				$branchBoss = User::find($activeChecklistProcess->branch_boss_id);
			}else{
				$branchBoss = User::where('brcode',$request->brcode)->where('user_type','BRANCH_BOSS')->first();
			}


			$branchBossID = !empty($branchBoss)?$branchBoss->id:Null;


			$activeChecklistProcess->inspector_description = $request->inspector_description;
			$activeChecklistProcess->inspector_program_number = $request->inspector_program_number;
			$activeChecklistProcess->inspector_program_date = $request->inspector_program_date;

			$activeChecklistProcess->branch_boss_id = $branchBossID;

			$activeChecklistProcess->save();
		  }
		  

		  if($respondAllRequiredQuestions && $request->store_step==2){
	  
			$activeChecklistProcess->ended_at = DB::raw('now()');
			$activeChecklistProcess->save();
			
		  }elseif(!$respondAllRequiredQuestions){
			$completedChecklistMessage = 'شما از تعداد '.$totalRequiredQuestionsCount." سوال مورد نیاز، به تعداد ".$selectedBranchStatistics[$request->brcode]." سوال پاسخ داده اید. برای ثبت نهایی چک لیست لازم است به تمامی سوالات پاسخ دهید.";
		  }
		  
	  }
	  
	  if($request->store_step==1){
	  
		return redirect(route('inspection.checklist.showChecklistOverView',[
                            'brcode'=>$request->brcode,
                            'inspection_date'=>$request->inspection_date,
							'rental_box'=>($branchScore->has_rental_box??0),
							'store_step' => '2'
                        ]));
	  
	  }else{
	  
		return redirect(route('inspection.checklist.show-final-page',[
                        'brcode'=>$request->brcode,
                        'period'=>$request->period,
						'inspection_date'=>$request->inspection_date,
						'showMessage' => 1
                      ]));
	  }
      
	  
	}
	
	
	
	public function showFinalPage(Request $request){
	
		$v = Validator::make($request->all(), [
		  'brcode' => 'required|integer',
		  'period' => 'required|integer',
		  'inspection_date' => 'required|max:20'
		]);

		if ($v->fails())
		{
		  return redirect()->back()->withErrors($v->errors());
		}
		
		
		
		
		$gregorianStartData = DateHelper::jalaliToGregorianString($request->inspection_date,"-");
		$activeChecklistProcess = $this->getActiveBranchInspectionProcess($request->brcode, $gregorianStartData);
	 

		$branch = Branch::where('brcode',$request->brcode)->first();
		$selectedBrName = $branch->brname;
	  
		$inspectionDateExploded = explode('-',$request->inspection_date);
		$period = $inspectionDateExploded[0];
	  
		$scoreStatusArray = $this->getInspectionScoreDetails($period, $request->brcode, $request->inspection_date);
	  
		$previousPeriod = $period-1;
		$previousPeriodScoreStatusArray = $this->getInspectionScoreDetails($previousPeriod, $request->brcode, $request->inspection_date);

		$inspectorsIDs = Answer::where('period',$period)
							->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
							->where('FK_BRC',$request->brcode)
							->groupBy('RESPONDER_USER')
							->get()
							->pluck('RESPONDER_USER')
							->toArray();
							
		$inspectors = User::whereIn('id',$inspectorsIDs)
							->get()
							->pluck('fullname')
							->toArray();
							
		if(!empty($activeChecklistProcess->branch_boss_id)){
			$branchBoss = User::find($activeChecklistProcess->branch_boss_id);
		}else{
			$branchBoss = User::where('brcode',$request->brcode)
								->first();
		}
		
		$branchBossFullName = !empty($branchBoss)?($branchBoss->fullname."(".$branchBoss->username.")"):'-';
	  
	  
		$branchScore = BranchScore::where('brcode',$request->brcode)
								->where('period',$period)
								->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
								->first();
								
		$inspectorResponse = $activeChecklistProcess->inspector_description;
		
		$inspectorProgramNumber = $activeChecklistProcess->inspector_program_number;
		
		$inspectorProgramDate = $activeChecklistProcess->inspector_program_date;
	  

	  
		$persianInspectionDate = strtr($period, array('0'=>'۰','1'=>'۱','2'=>'۲','3'=>'۳','4'=>'۴','5'=>'۵','6'=>'۶','7'=>'۷','8'=>'۸','9'=>'۹'));
		$persianInspectionDate = str_replace('-','/',$persianInspectionDate);

	  
		$rentalBox = $request->rental_box??0;
		
		

		$selectedBranchStatistics = GeneralHelper::getBranchesStatisticsByAllStatuses(Branch::where('brcode',$request->brcode)->get()->pluck('brcode')->toArray());

		$totalRequiredQuestionsCount = Question::select();
		if($branch->arzi==0){
			$totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('ARZI_QUESTION',0);
		}

		if($branch->has_rental_box==0){
			$totalRequiredQuestionsCount = $totalRequiredQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
		}
	
		$totalRequiredQuestionsCount = $totalRequiredQuestionsCount->count();

	  
		$branchExtraInfo = \App\Models\BranchExtraInfo::where('brcode',$request->brcode)->first();


		
		
		$checklist = Answer::where('FK_BRC',$request->brcode)
							->where('PERIOD',$request->period)
							->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
							->get();
							
		
		$checklistArray = [];
		foreach($checklist as $key=>$checklistItem){
		
			$questionText = $checklistItem->Question()->get()->first();
			
			
			$questionAnswerTexts = $checklistItem->answerTexts()->get();
			$questionAnswerChoices = $checklistItem->answerChoices()->first();
			$questionAnswerDocuments = $checklistItem->answerDocumentations()->get();
			
			
			$checklistArray[] = [
				'questionText' => $questionText,
				'questionAnswerTexts' => $questionAnswerTexts,
				'questionAnswerChoices' => $questionAnswerChoices,
				'questionAnswerDocuments' => $questionAnswerDocuments
			];
		
		}
							
		$message = NULL;
		if($request->showMessage){
			$message = "اطلاعات مورد نظر با موفقیت برای شعبه ".$branch->brname." با کد ".$branch->brcode." در بازه ".$request->period." ثبت شد!";
		}
		
		$explodedDate = explode(" ",$activeChecklistProcess->started_at);
		$inspectionStartDate="-";
		if(!empty($explodedDate[0])){
			$inspectionStartDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
		}

		$explodedDate = explode(" ",$activeChecklistProcess->ended_at);
		$inspectionEndDate = "-";
		if(!empty($explodedDate[0])){
			$inspectionEndDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
		}
		
		
		//BEGIN GET SUMMARY OF CATEGORY SCORES
		$sumOfallGroupsScore = QuestionGroupBranchScore::where('brcode', $request->brcode)
								->where('period', $request->period)
								->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
								->sum('score');
								
		
		
		
		$questionGroupSummaryStats = [];
		
		$allQuestionGroups = QuestionGroup::select('*')
								->orderBy('order','ASC')
								->get();
		

		$sumOfQuestionsCount = 0;
		$sumOfReviewedToTotalRatio = 0;
		$sumOfCategoryTotalScoreValue = 0;
		foreach($allQuestionGroups as $questionGroupRow){
		
			$groupScore = QuestionGroupBranchScore::where('question_group_id', $questionGroupRow->id)
								->where('brcode', $request->brcode)
								->where('period', $request->period)
								->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
								->sum('score');
			
			$sumOfCategoryTotalScoreValue += $groupScore;
								
			$questionGroupSummaryStats[$questionGroupRow->id] = [
				'group_title' => $questionGroupRow->group_title,
				'category_title' => $questionGroupRow->category()->TITLE,
				'reviewed_count' => 0,
				'total_count' => 0,
				'category_total_score_value' => $groupScore,
				'category_total_score_percent' => round( ((100/$sumOfallGroupsScore)*$groupScore) ,2),
			];
			
			
			$allGroupQuestions = Question::where('FK_QUESTION_GROUP',$questionGroupRow->id)->get();
			
			$questionGroupSummaryStats[$questionGroupRow->id]['group_questions_count'] = $allGroupQuestions->count();
			
			$sumOfQuestionsCount += $allGroupQuestions->count();
			
			foreach($allGroupQuestions as $questionRow){
				$questionAnswerIDs = Answer::where('FK_QUE',$questionRow->ID)
										->where('FK_BRC',$branch->brcode)
										->where('CHECKLIST_PROCESS_ID',$activeChecklistProcess->id)
										->get()->pluck('ID')->toArray();
										
				
				$questionAnswerReviewedCounts  = AnswerText::whereIn('FK_ANS', $questionAnswerIDs)
															->where('FK_ANS_ITEM_ID',21)
															->sum('ANSWER_TEXT');
															
				$questionGroupSummaryStats[$questionGroupRow->id]['reviewed_count'] += $questionAnswerReviewedCounts;
														
				$questionAnswerTotalCounts  = AnswerText::whereIn('FK_ANS', $questionAnswerIDs)
															->where('FK_ANS_ITEM_ID',20)
															->sum('ANSWER_TEXT');
															
				$questionGroupSummaryStats[$questionGroupRow->id]['total_count'] += $questionAnswerTotalCounts;
				
			}
			
			$questionGroupSummaryStats[$questionGroupRow->id]['reviewed_to_total_ratio'] = ($questionGroupSummaryStats[$questionGroupRow->id]['total_count']>0)?
																								(round($questionGroupSummaryStats[$questionGroupRow->id]['reviewed_count']/$questionGroupSummaryStats[$questionGroupRow->id]['total_count'],2)):0;
			$sumOfReviewedToTotalRatio += $questionGroupSummaryStats[$questionGroupRow->id]['reviewed_to_total_ratio'];
			
			
			
		}
		

		$questionGroupSummaryStatsSum = [
			'all_question_groups_count' => $allQuestionGroups->count(),
			'group_questions_count' => $sumOfQuestionsCount,
			'reviewed_to_total_ratio' => $sumOfReviewedToTotalRatio,
			'category_total_score_value' => $sumOfCategoryTotalScoreValue,
		];
		//END GET SUMMARY OF CATEGORY SCORES
		
		
		$branchclassification = \App\Models\BranchClassification::where('brcode',$request->brcode)->first();
		
		$darajehTitle = $branchclassification->darajeh;
		if(!empty($this->darajehTitles[$branchclassification->darajeh])){
			$darajehTitle = $this->darajehTitles[$branchclassification->darajeh];
		}
		
		
		$responseData = [
						'selectedBrCode' => $request->brcode,
						'selectedBrName' => $selectedBrName,
						'inspectionDate' => $request->inspection_date,
						'period' => $period,
						'scoreStatusArray' => $scoreStatusArray,
						'previousPeriodScoreStatusArray' => $previousPeriodScoreStatusArray,
						'inspectors' => $inspectors,
						'branchBoss' => $branchBossFullName,
						'inspectorResponse' => $inspectorResponse,
						'inspectorProgramNumber' => $inspectorProgramNumber,
						'inspectorProgramDate' => $inspectorProgramDate,
						'rentalBox' => $rentalBox,
						'checklistArray'=>$checklistArray,
						'persianInspectionDate' => $persianInspectionDate,
						'selectedBranch' => $branch,
						'message' => $message,
						'inspectionStartDate' => $inspectionStartDate,
						'inspectionEndDate' => $inspectionEndDate,
						'branchExtraInfo' => $branchExtraInfo,
						'branchclassification' => $branchclassification,
						'questionGroupSummaryStats' => $questionGroupSummaryStats,
						'darajehTitle' => $darajehTitle,
						'questionGroupSummaryStatsSum' => $questionGroupSummaryStatsSum,
						'exportPDF_url' => ("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."&export_pdf=1")
						];

		
		
		if($request->export_pdf){

			$pdf = Pdf::loadView('pages.final-page-pdf', $responseData);
			return $pdf->download('inspection-branch'.$request->brcode.'-'.time().'.pdf');
		
		}
		
		return view('pages.final-page',$responseData);
	
	}
	
	
	public function getNationalCodeByAccountNumber(Request $request){
	
		if(!$request->ajax()) {
			//return "invalid request!";
		}
		
		$v = Validator::make($request->all(), [
		  'account_number' => 'required|min:4',
		]);

		if ($v->fails())
		{
			return response()->json([
				'status' => 'bad-request', 
				'data' =>[], 
				'message' => 'درخواست نامعتبر است!'
			], 400);
		}
		
		
		$requestURL = 'http://'.config('app.inspection-core.base_url').'/'.config('app.inspection-core.get_national_code_by_account_number_uri');
		$accountData = file_get_contents($requestURL."?account_number=".$request->account_number);
		$accountData = json_decode($accountData,true);
		
		
		return response()->json([
			'status' => 'success', 
			'data' =>['national_code'=>$accountData['national_code'],'first_name'=>$accountData['first_name'],'last_name'=>$accountData['last_name'],'customer_type'=>$accountData['customer_type']], 
			'message' => ''
		], 200);
		
		
	
	}
	
	
	public function getPersonnelByPersonnelCode(Request $request){
	
		if(!$request->ajax()) {
			return "invalid request!";
		}
		
		$v = Validator::make($request->all(), [
		  'personnel_code' => 'required|integer',
		]);

		if ($v->fails())
		{
			return response()->json([
				'status' => 'bad-request', 
				'data' =>[], 
				'message' => 'درخواست نامعتبر است!'
			], 400);
		}
		
		
		$requestURL = 'http://'.config('app.rosha-core.base_url').'/'.config('app.rosha-core.get_personnel_by_personnel_code');
		$personnelData = file_get_contents($requestURL."?personnel_code=".$request->personnel_code);
		$personnelData = json_decode($personnelData,true);
		
		
		return response()->json([
			'status' => 'success', 
			'data' =>['festno'=>$personnelData['festno'],'flname'=>$personnelData['flname'],'fmadrak'=>$personnelData['fmadrak'],'festdate'=>$personnelData['festdate']], 
			'message' => ''
		], 200);
		
		
	
	}


}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\User;
use App\Models\Branch;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
       $validator =  $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
   
    
        $credentials = $request->only('username', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->intended(route('pages.dashboard'))
                        ->withSuccess('با موفقیت وارد شدید');
        }
        $validator['usernamePassword'] = 'نام کاربری یا رمز عبور اشتباه است!';
        return redirect("login")->withErrors($validator);
    }

    public function logout() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
	
	
	public function loginFromRosha(Request $request){

		$brcode = base64_decode($request->brcode);
		$username = base64_decode($request->username);
		if(empty($brcode) || empty($username)){
			die("INVALID REQUEST");
		}
	
		$userInMoneylaun = User::where('username',$username)->where('user_type','BRANCH_BOSS')->first();
		if(empty($userInMoneylaun)){
		
			$branch = Branch::where('brcode',$brcode)->firstOrFail();
			
			$newUser = new User();
			
			$newUser->fullname = 'رییس شعبه '.$brcode;
			$newUser->username = $username;
			$newUser->password = bcrypt($username."123456");
			$newUser->sarcode = $branch->sarcode;
			$newUser->brcode = $brcode;
			$newUser->user_type = "BRANCH_BOSS";
			
			$newUser->save();
			
			$userInMoneylaun = User::where('username',$username)->where('user_type','BRANCH_BOSS')->first();
			
		}
		
		Auth::loginUsingId($userInMoneylaun->id);
		
		return Redirect(route('pages.dashboard'));
	
	}
	
	
	public function loginFromInspectionSystem(Request $request){
	
	
		$sarparastiCode = base64_decode($request->sarparasti_code);
		$username = base64_decode($request->username);
		if(empty($sarparastiCode) || empty($username)){
			die("INVALID REQUEST");
		}
		
		if($request->user_type=='SARPARASTI_INSPECTOR'){
			$userInMoneylaun = User::where('username',$username)->where('user_type','SARPARASTI_INSPECTOR')->first();
			
			if(empty($userInMoneylaun)){
			
				$sarparasti = Branch::where('sarcode',$sarparastiCode)->first();
				if(empty($sarparasti)){
					return Redirect(route('pages.dashboard'));
				}
				
				$newUser = new User();
				
				$newUser->fullname = !empty($request->fullname) ? (base64_decode($request->fullname)) : ('بازرس اداره امور '.$sarparasti->sarname);
				$newUser->username = $username;
				$newUser->password = bcrypt($username."123456");
				$newUser->sarcode = $sarparasti->sarcode;
				$newUser->brcode = NULL;
				$newUser->user_type = "SARPARASTI_INSPECTOR";
				
				$newUser->save();
				
				$userInMoneylaun = User::where('username',$username)->where('user_type','SARPARASTI_INSPECTOR')->first();
				
			}
			
			
		}
		Auth::loginUsingId($userInMoneylaun->id);
		return Redirect(route('pages.dashboard'));
	
	}
	
	
}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LookupQuestion;
use App\Models\Branch;
use App\Models\Answer;
use App\Models\AnswerText;
use App\Models\AnswerDocumentation;
use App\Models\Question;
use App\Models\QuestionGroup;
use App\Models\CategoryBranchScore;
use App\Models\QuestionGroupBranchScore;
use App\Models\BranchScore;
use App\Models\UserSarparasti;
use App\Models\ChecklistProcess;
use App\Models\DocumentFile;
use App\User;
use App\Helpers\DateHelper;
use App\Helpers\GeneralHelper;
use Validator;
use DB;

class ReportController extends Controller
{


	public function __constract(){
	}
	

    public function showReportPage(){

	  if(auth()->user()->user_type=='ADMIN'){
		$sarparasties = Branch::getAllSaraparasties();
	  }else if(auth()->user()->user_type=='SARPARASTI_INSPECTOR' || auth()->user()->user_type=='BRANCH_BOSS'){
		$sarparasties = Branch::getSaraparasti(auth()->user()->sarcode);
	  }else if(auth()->user()->user_type=='GENERAL_INSPECTOR'){
		$sarcodes = UserSarparasti::where('user_id',auth()->user()->id)->get()->pluck('sarcode')->toArray();
		$sarparasties = Branch::getSaraparasti($sarcodes);
	  }
	  
	  $reportData = [];
	  foreach($sarparasties as $sarparastiItem){
		
		$accessibleBranchesList = GeneralHelper::getSarparastiBranches($sarparastiItem->sarcode);
		
		$accessibleBrCodes = [];
		foreach($accessibleBranchesList as $brRow){
			$accessibleBrCodes[] = $brRow['brcode'];
		}
		$sarparastiStatisticsBasedOnBranches = GeneralHelper::getBranchesStatisticsByAllStatuses($accessibleBrCodes);


		
		$completedBranches = 0;
		$inCompletingBranches = 0;
		foreach($accessibleBranchesList as $branchItem){
		
			$totalQuestionsCount = Question::select();
			if($branchItem['arzi']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('ARZI_QUESTION',0);
			}
			
			if($branchItem['has_rental_box']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
			}
			
			$totalQuestionsCount = $totalQuestionsCount->count();
			
			
			
			if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]==$totalQuestionsCount){
				$completedBranches++;
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]<$totalQuestionsCount){
				$inCompletingBranches++;
			}

			
			
			
		}
		
		$reportData[$sarparastiItem->sarcode] = [
													'sarparasti_code' => $sarparastiItem->sarcode,
													'sarparasti_name' => $sarparastiItem->sarname,
													'branches_count' => count($accessibleBranchesList),
													'complete_branches_count' => $completedBranches,
													'in_completing_branches_count' => $inCompletingBranches,
												];
												
		
	  }
	  
	  
	  
	  
	  
	  
      return view('pages.report-main',['reportData' => $reportData]);

    }
	
	
	
	
	public function getSarparastiBranchesReport(Request $request){
	
		$inputData = [
			'sarcode' => $request->sarcode,
		];

		$v = Validator::make($inputData, [
			'sarcode' => 'required|integer',
		]);
		
		$accessibleSarparasties = [];
		if(auth()->user()->user_type=='ADMIN'){
			$accessibleSarparasties = Branch::getAllSaraparasties();
		}else if(auth()->user()->user_type=='SARPARASTI_INSPECTOR' || auth()->user()->user_type=='BRANCH_BOSS'){
			$accessibleSarparasties = Branch::getSaraparasti(auth()->user()->sarcode);
		}else if(auth()->user()->user_type=='GENERAL_INSPECTOR'){
			$accessibleSarparasties = UserSarparasti::where('user_id',auth()->user()->id)->get();
		  }

		if ($v->fails() || !in_array($request->sarcode,$accessibleSarparasties->pluck('sarcode')->toArray()))
		{
			return redirect(route('inspection.report.show-report-page'))->withErrors(['message'=>'خطایی پیش آمده است!']);
		}
		
		
		$reportData = [];
		$accessibleBranchesList = GeneralHelper::getSarparastiBranches($request->sarcode);
		$accessibleBranchCodes = [];
		foreach($accessibleBranchesList as $branchRow){
			$accessibleBranchCodes[] = $branchRow['brcode'];
		}
		
		$sarparastiStatisticsBasedOnBranches = GeneralHelper::getBranchesStatisticsByAllStatuses($accessibleBranchCodes);
		
		
		$branchesReport = [];
		foreach($accessibleBranchesList as $branchItem){
		
			$totalQuestionsCount = Question::select();
			if($branchItem['arzi']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('ARZI_QUESTION',0);
			}

			if($branchItem['has_rental_box']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
			}

			$totalQuestionsCount = $totalQuestionsCount->count();

			$branchChecklistStatus = "";
			$branchChecklistStatusTitle = "";
			
			if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]==0){
				$branchChecklistStatus = "NOT_COMPLETED";
				$branchChecklistStatusTitle = "شروع نشده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]==$totalQuestionsCount){
				$branchChecklistStatus = "COMPLETED";
				$branchChecklistStatusTitle = "تکمیل شده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]<$totalQuestionsCount){
				$branchChecklistStatus = "IN_COMPLETING";
				$branchChecklistStatusTitle = "در حال تکمیل";
			}else{
				$branchChecklistStatus = "UNKNOWN";
				$branchChecklistStatusTitle = "نامشخص";
			}
			
			
			$branchesReport[$branchItem['brcode']] = [
														'branch_code' => $branchItem['brcode'],
														'branch_name' => $branchItem['brname'],
														'total_questions_scount' => $totalQuestionsCount,
														'branch_checklist_status_title' => $branchChecklistStatusTitle,
														'is_arzi' => ($branchItem['arzi']==1)?'ارزی':'ریالی',
														'has_rental_box' => ($branchItem['has_rental_box']==1)?'دارد':'ندارد',
														'respond_questions_count' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]
														
													];
		
		
		}
		
	  
		return view('pages.report-branches-checklist',['reportData' => $branchesReport]);
	  

	}
	
	
	public function showReportsList(Request $request){
		return view('pages.reports.list');
	}
	
	private function getAllBranches($sarparasties){
	
		$branchesList = [];
		foreach($sarparasties as $sarparastiItem){
			$branches = Branch::select('id','brcode', 'brname', 'arzi', 'sarcode')
								->where('sarcode',$sarparastiItem->sarcode);
			
			if(auth()->user()->user_type=='BRANCH_BOSS'){
				$branches = $branches->where('brcode',auth()->user()->brcode);
			}
			
			$branchesList = array_merge($branchesList, $branches->get()->toArray());
		}
						
		return $branchesList;
		
	}
	
	
	public function showInspectionRecords(Request $request){
	
	

	
		$accessibleSarparasties = [];
		if(auth()->user()->user_type=='ADMIN'){
			$accessibleSarparasties = Branch::getAllSaraparasties();
		}else if(auth()->user()->user_type=='SARPARASTI_INSPECTOR' || auth()->user()->user_type=='BRANCH_BOSS'){
			$accessibleSarparasties = Branch::getSaraparasti(auth()->user()->sarcode);
		}else if(auth()->user()->user_type=='GENERAL_INSPECTOR'){
			$accessibleSarparasties = UserSarparasti::where('user_id',auth()->user()->id)->get();
		}

		
		$allAccessibleBranches = [];
		foreach($accessibleSarparasties as $sarparasti){
			$accessibleSarparastiBranchesList = GeneralHelper::getSarparastiBranches($sarparasti->sarcode);
			
			$allAccessibleBranches = array_merge($allAccessibleBranches, $accessibleSarparastiBranchesList);
			
		}
		$allBrCodes = [];
		foreach($allAccessibleBranches as $branchRow){
			$allBrCodes[] = $branchRow['brcode'];
		}
		
		if(!in_array($request->brcode,$allBrCodes)){
			return redirect()->back()->withErrors([]);
		}
		
		$sarparastiStatisticsBasedOnBranches = GeneralHelper::getInspectedBranchesStatisticsOrdered([$request->brcode]);
		$reportData = [];
		foreach($sarparastiStatisticsBasedOnBranches as $branchCode => $branchInspectionRow){
		
			$branchItem = Branch::where('brcode',$branchCode)->first();
		
			$totalQuestionsCount = Question::select();
			if($branchItem['arzi']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('ARZI_QUESTION',0);
			}

			if($branchItem['has_rental_box']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
			}

			$totalQuestionsCount = $totalQuestionsCount->count();

			$branchChecklistStatus = "";
			$branchChecklistStatusTitle = "";
			
			if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']==0){
				$branchChecklistStatus = "NOT_COMPLETED";
				$branchChecklistStatusTitle = "شروع نشده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']==$totalQuestionsCount){
				$branchChecklistStatus = "COMPLETED";
				$branchChecklistStatusTitle = "تکمیل شده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']<$totalQuestionsCount){
				$branchChecklistStatus = "IN_COMPLETING";
				$branchChecklistStatusTitle = "در حال تکمیل";
			}else{
				$branchChecklistStatus = "UNKNOWN";
				$branchChecklistStatusTitle = "نامشخص";
			}
			
			
			$reportData[$branchItem['brcode']] = [
														'branch_code' => $branchItem['brcode'],
														'branch_name' => $branchItem['brname'],
														'total_questions_scount' => $totalQuestionsCount,
														'branch_checklist_status_title' => $branchChecklistStatusTitle,
														'is_arzi' => ($branchItem['arzi']==1)?'ارزی':'ریالی',
														'has_rental_box' => ($branchItem['has_rental_box']==1)?'دارد':'ندارد',
														'respond_questions_count' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count'],
														'started_at_jalali' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['started_at_jalali'],
														'ended_at_jalali' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['ended_at_jalali'],
														'period' => 1403 //TODO: should be read from future filters
													];
		
		
		}
		
	  
		return view('pages.reports.records',['reportData' => $reportData, 'brcode' => $request->brcode]);
	
	
	
	
	}
	
	
	
	
	public function showResourcesList(){
	
		$resources = DocumentFile::orderBy('document_order')->get();
	
		return view('pages.resources.list',['resources'=>$resources]);
	}
	
	
	
	
	
	
	
	
	
	public function showLatestInspections(){

	  
		$accessibleSarparasties = [];
		if(auth()->user()->user_type=='ADMIN'){
			$accessibleSarparasties = Branch::getAllSaraparasties();
		}else if(auth()->user()->user_type=='SARPARASTI_INSPECTOR' || auth()->user()->user_type=='BRANCH_BOSS'){
			$accessibleSarparasties = Branch::getSaraparasti(auth()->user()->sarcode);
		}else if(auth()->user()->user_type=='GENERAL_INSPECTOR'){
			$accessibleSarparasties = UserSarparasti::where('user_id',auth()->user()->id)->get();
		}

		
		
		
		$allAccessibleBranches = [];
		foreach($accessibleSarparasties as $sarparasti){
			$accessibleSarparastiBranchesList = GeneralHelper::getSarparastiBranches($sarparasti->sarcode);
			
			$allAccessibleBranches = array_merge($allAccessibleBranches, $accessibleSarparastiBranchesList);
			
		}
		
		$allAccessibleBranchCodes = [];
		foreach($allAccessibleBranches as $accBranchRow){
			$allAccessibleBranchCodes[] = $accBranchRow['brcode'];
		}
		
		$sarparastiStatisticsBasedOnBranches = GeneralHelper::getInspectedBranchesStatisticsOrdered($allAccessibleBranchCodes);
		$reportData = [];
		foreach($sarparastiStatisticsBasedOnBranches as $branchCode => $branchInspectionRow){
		
			$branchItem = Branch::where('brcode',$branchCode)->first();
		
			$totalQuestionsCount = Question::select();
			if($branchItem['arzi']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('ARZI_QUESTION',0);
			}

			if($branchItem['has_rental_box']==0){
				$totalQuestionsCount = $totalQuestionsCount->where('FK_QUESTION_GROUP','<>',9); //is not rental box group
			}

			$totalQuestionsCount = $totalQuestionsCount->count();

			$branchChecklistStatus = "";
			$branchChecklistStatusTitle = "";
			
			if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']==0){
				$branchChecklistStatus = "NOT_COMPLETED";
				$branchChecklistStatusTitle = "شروع نشده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']==$totalQuestionsCount){
				$branchChecklistStatus = "COMPLETED";
				$branchChecklistStatusTitle = "تکمیل شده";
			}else if($sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']>0 && $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count']<$totalQuestionsCount){
				$branchChecklistStatus = "IN_COMPLETING";
				$branchChecklistStatusTitle = "در حال تکمیل";
			}else{
				$branchChecklistStatus = "UNKNOWN";
				$branchChecklistStatusTitle = "نامشخص";
			}
			
			
			$reportData[$branchItem['brcode']] = [
														'branch_code' => $branchItem['brcode'],
														'branch_name' => $branchItem['brname'],
														'total_questions_scount' => $totalQuestionsCount,
														'branch_checklist_status_title' => $branchChecklistStatusTitle,
														'is_arzi' => ($branchItem['arzi']==1)?'ارزی':'ریالی',
														'has_rental_box' => ($branchItem['has_rental_box']==1)?'دارد':'ندارد',
														'has_rental_box_value' => $branchItem['has_rental_box'],
														'respond_questions_count' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['answers_count'],
														'started_at_jalali' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['started_at_jalali'],
														'ended_at_jalali' => $sarparastiStatisticsBasedOnBranches[$branchItem['brcode']]['ended_at_jalali'],
														'period' => 1403 //TODO: should be read from future filters
														
													];
		
		
		}
		
	  
		return view('pages.report-latest-inspections',['reportData' => $reportData]);
	  
	  

    }
	
	
	
	public function showTimeBasedChart(Request $request){
	
	
		$inputData = $request->all();
		
		$v = Validator::make($inputData, [
			'criteria' => 'required',
			'based_data' => 'required',
		]);
	
		$getChartDataMethodName = "getChart".ucfirst($request->based_data)."DataBasedOn".ucfirst($request->criteria);
		$chartData = call_user_func([$this,$getChartDataMethodName],$request);


		$chart1 = \Chart::title([
            'text' => $chartData['chart_title'],
        ])
        ->chart([
            'type'     => 'column',
            'renderTo' => 'chart1',
        ])
        ->subtitle([
            'text' => $chartData['chart_sub_title'],
        ])
        ->xaxis([
            'categories' => $chartData['categories'],
            'labels'     => [
                'rotation'  => 15,
                'align'     => 'top',
                'formatter' => 'startJs:function(){return this.value}:endJs',
            ],
        ])
        ->yaxis([
            'title' => [
                'text' => $chartData['y_axis_title'],
            ]
        ])
        ->legend([
            'layout'        => 'vertical',
            'align'         => 'right',
            'verticalAlign' => 'middle',
        ])
        ->series($chartData['data_array'])
        ->display();
		
		
		return view('pages.reports.charts.chart_base_type1', [
			'chart1' => $chart1,
			'basedData' => $request->based_data,
			'criteria' => $request->criteria,
			'data_list' => $chartData['dataList']??[]
		]);
		

	}
	
	                 
	public function getChartBankDataBasedOnScore($request){
		$bankScoresBasedOnTime = DB::table('tb_checklist_process')
						->select(DB::raw('SUM(score) as total_score'), DB::raw('SUBSTRING(started_at_jalali, 1, 7) as process_month'))
						->join('tb_branch_score', 'tb_branch_score.CHECKLIST_PROCESS_ID', '=', 'tb_checklist_process.id');
						
						
		if(!empty($request->from_date)){
		
			$gregorianFromDate = DateHelper::jalaliToGregorianString($request->from_date,"-");
		
			$bankScoresBasedOnTime = $bankScoresBasedOnTime->where('started_at','>=',$gregorianFromDate);
			
		}
		
		if(!empty($request->to_date)){
		
			$gregorianToDate = DateHelper::jalaliToGregorianString($request->to_date,"-");
		
			$bankScoresBasedOnTime = $bankScoresBasedOnTime->where('started_at','<=',$gregorianToDate);
		}
		
		$bankScoresBasedOnTime = $bankScoresBasedOnTime->groupBy(DB::raw('process_month'))
						->get();
						
						
	
		
		$dataArray = [];
		$categories = [];
		foreach($bankScoresBasedOnTime as $dataRow){
		
			if(!in_array($dataRow->process_month, $categories)){
				$categories[] = $dataRow->process_month;
			}

			if(!isset($dataArray[$dataRow->process_month])){
				$dataArray[$dataRow->process_month] = (object)[
					'name' => $dataRow->process_month, 
					'data' => [((int)$dataRow->total_score)],
					'color' => '#0c2959'
				];
			}else{
				$dataArray[$dataRow->process_month]->data = array_merge($dataArray[$dataRow->process_month]->data,[(int)$dataRow->total_score]);
			}
			
		}
		
		
		foreach($dataArray as $processMonth=>$dataRow){
			if(count($dataRow->data)<count($categories)){
				for($i=1; $i<=(count($categories)-count($dataRow->data)); $i++){
					$dataArray[$processMonth]->data = array_merge($dataArray[$processMonth]->data,[0]);
				}
			}
		}
		
		
		$transformedDataArray = [];
		foreach($dataArray as $sarcode=>$dataRow){
			$transformedDataArray[] = $dataRow;
		}
		
		
		
		return [
			'chart_title' => 'نمودار مجموع امتیاز کل بانک بر اساس زمان',
			'chart_sub_title' => 'کل بانک',
			'y_axis_title' => 'score',
			'data_array' => $transformedDataArray,
			'categories' => $categories
			
		];

		
	}
	
	
	public function getChartSarparastiesDataBasedOnScore($request){
	
		$sarparastiesScoresBasedOnTime = DB::table('tb_checklist_process')
						->select(
									DB::raw('branches.sarname'),
									DB::raw('branches.sarcode'),
									DB::raw('SUM(score) as total_score'), 
									DB::raw('SUBSTRING(started_at_jalali, 1, 7) as process_month'),
									DB::raw('Max(started_at_jalali) as max_process_month'),
									DB::raw('Max(tb_checklist_process.updated_at) as max_updated_at'),
								);
		
		if(!empty($request->from_date)){
		
			$gregorianFromDate = DateHelper::jalaliToGregorianString($request->from_date,"-");
		
			$sarparastiesScoresBasedOnTime = $sarparastiesScoresBasedOnTime->where('started_at','>=',$gregorianFromDate);
			
		}
		
		if(!empty($request->to_date)){
		
			$gregorianToDate = DateHelper::jalaliToGregorianString($request->to_date,"-");
		
			$sarparastiesScoresBasedOnTime = $sarparastiesScoresBasedOnTime->where('started_at','<=',$gregorianToDate);
		}
		
		
		$sarparastiesScoresBasedOnTime = $sarparastiesScoresBasedOnTime->join('tb_branch_score', 'tb_branch_score.CHECKLIST_PROCESS_ID', '=', 'tb_checklist_process.id')
						->join('branches', 'branches.brcode', '=', 'tb_checklist_process.brcode')
						->groupBy(DB::raw('process_month'),DB::raw('sarcode'))
						->get();
		
		
		$dataArray = [];
		$categories = [];
		$dataList = [];
		foreach($sarparastiesScoresBasedOnTime as $dataRow){
		
			$maxUpdatedDate = $dataRow->max_updated_at;
			$explodedMaxUpdateDate = explode(' ',$maxUpdatedDate);
			$explodedMaxUpdateDate = explode('-',$explodedMaxUpdateDate[0]);
			$dataList[] = [
				'sarname' => $dataRow->sarname,
				'sarcode' => $dataRow->sarcode,
				'total_score' => [((int)$dataRow->total_score)],
				'sarname' => $dataRow->sarname,
				'sarcode' => $dataRow->sarcode,
				'total_score' => $dataRow->total_score,
				'process_month' => $dataRow->process_month,
				'max_process_month' => $dataRow->max_process_month,
				'max_updated_at' => implode('/',DateHelper::gregorian_to_jalali($explodedMaxUpdateDate[0],$explodedMaxUpdateDate[1],$explodedMaxUpdateDate[2]))
			];
		
		
			if(!in_array($dataRow->process_month, $categories)){
				$categories[] = $dataRow->process_month;
			}

			if(!isset($dataArray[$dataRow->sarcode])){
				$dataArray[$dataRow->sarcode] = (object)[
					'name' => $dataRow->sarname, 
					'data' => [((int)$dataRow->total_score)],
					'color' => '#0c2959'
				];
			}else{
				$dataArray[$dataRow->sarcode]->data = array_merge($dataArray[$dataRow->sarcode]->data,[(int)$dataRow->total_score]);
			}
			
		}
		
		foreach($dataArray as $sarcode=>$dataRow){
			if(count($dataRow->data)<count($categories)){
				for($i=1; $i<=(count($categories)-count($dataRow->data)); $i++){
					$dataArray[$sarcode]->data = array_merge($dataArray[$sarcode]->data,[0]);
				}
			}
		}
		
		
		$transformedDataArray = [];
		foreach($dataArray as $sarcode=>$dataRow){
			$transformedDataArray[] = $dataRow;
		}

		
		return [
			'chart_title' => 'نمودار مجموع امتیاز هر اداره امور بر اساس زمان',
			'chart_sub_title' => 'ادارات امور',
			'y_axis_title' => 'score',
			'data_array' => $transformedDataArray,
			'categories' => $categories,
			'dataList' => $dataList
			
		];
	
	}
	
}


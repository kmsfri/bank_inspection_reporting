<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ChoiceDocuments extends Component
{

    public $questionItem;
    public $choiceItem;
    public $countFrom;
	public $documentsData;
	public $disabled;
	public $disabledBrBossFields;
	
	

    /**
     * Create a new component instance.
     *
     * @return void
     */
     public function __construct($countFrom,$questionItem, $choiceItem, $documentsData, $disabled, $disabledBrBossFields)
     {
         $this->questionItem = $questionItem;
         $this->choiceItem = $choiceItem;
         $this->countFrom = $countFrom;
		 $this->documentsData = $documentsData??NULL;
		 $this->disabled = $disabled??false;
		 $this->disabledBrBossFields = $disabledBrBossFields??false;
     }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.choice-documents');
    }
}


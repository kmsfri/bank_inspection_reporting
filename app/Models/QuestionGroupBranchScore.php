<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionGroupBranchScore extends Model
{
    protected $table = "tb_question_group_branch_score";
	
	public $timestamps = false;

}


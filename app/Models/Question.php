<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "TB_QUESTION";

    public function answerPatterns(){
      return $this->belongsToMany(AnswerPattern::class, 'QUESTIONANSWER', 'FK_QUE', 'FK_ANSPTN', 'ID');
    }



    public function subQuestions(){
      return $this->hasMany(Question::class, 'FK_QUESTION','ID');
    }
	
	public function questionTypeCategory(){
      return $this->belongsTo(LookupQuestion::class, 'QUESTION_TYPE_CATEGORY_ID','ID');
    }
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    protected $table = "TB_QUESTION_GROUPS";
	
	
	public function category(){
		return $this->belongsTo(LookupQuestion::class, 'category_key','CODE')->first();
	}

}


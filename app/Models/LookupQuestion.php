<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LookupQuestion extends Model
{
    protected $table = "TB_LOOKUPQUE";


    public function questions(){
      return $this->hasMany(Question::class, 'FK_LKPQUE_PARENT', 'ID');
    }

}

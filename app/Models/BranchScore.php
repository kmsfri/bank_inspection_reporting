<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchScore extends Model
{
    protected $table = "tb_branch_score";
	
	public $timestamps = false;

}


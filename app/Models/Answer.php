<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "TB_ANSWER";

    public function answerTexts(){
      return $this->hasMany(AnswerText::class, 'FK_ANS','ID');
    }
	
	
	public function answerDocumentations(){
      return $this->hasMany(AnswerDocumentation::class, 'FK_ANS','ID');
    }
	
	
	public function Question(){
		return $this->belongsTo(Question::class, 'FK_QUE','ID');
	}
	
	
	public function answerChoices(){
		return $this->belongsTo(AnswerItem::class, 'SELECTED_ANSWER','ID');
	}


}


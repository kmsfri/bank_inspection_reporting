<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerText extends Model
{
    protected $table = "TB_ANSWER_TEXT";

	public $timestamps = false;
	
	
	public function AnswerItem(){
		return $this->belongsTo(AnswerItem::class, 'FK_ANS_ITEM_ID','ID');
	}
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerPattern extends Model
{
    protected $table = "TB_ANSWERPATTERN";

    public function answers(){
      return $this->belongsToMany(AnswerItem::class, 'TB_PATTERN_ITEM', 'FK_ANSPTN', 'FK_ANSITM', 'ID','ID')
                  ->with('subAnswers');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table = "BRANCHES";


	
	public static function getAllSaraparasties(){
		return self::select(['sarcode', 'sarname'])->where('sarcode','<>',290)->groupBy(['sarcode'])->get();
	}
	
	public static function getSaraparasti($sarcode){
		if(is_array($sarcode)){
			return self::select(['sarcode', 'sarname'])->whereIn('sarcode',$sarcode)->groupBy(['sarcode'])->get();
		}else{
			return self::select(['sarcode', 'sarname'])->where('sarcode',$sarcode)->groupBy(['sarcode'])->get();
		}
	}

}


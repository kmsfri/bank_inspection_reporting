<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerDocumentation extends Model
{
    protected $table = "TB_ANSWER_DOCUMENTATION";


    public $timestamps = false;
}


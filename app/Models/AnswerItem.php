<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerItem extends Model
{
    protected $table = "TB_ANSWER_ITEM";

    public function subAnswers(){
      return $this->hasMany(AnswerItem::class, 'FK_ANSITM_ANS_PARENT_TYPE','ID');
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryBranchScore extends Model
{
    protected $table = "tb_category_branch_score";
	
	public $timestamps = false;

}


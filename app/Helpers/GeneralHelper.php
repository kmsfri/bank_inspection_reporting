<?php

namespace App\Helpers;


use App\Helpers\DateHelper;
use App\Models\ChecklistProcess;
use App\Models\Branch;
use App\Models\Answer;

class GeneralHelper
{

	public static function getBranchesStatistics($branches){
	
		
		
	
		$checklistLatestProcess = ChecklistProcess::selectRaw('id, max(brcode) as brcode, max(started_at) as started_at')
														->whereIn('brcode',$branches)
														->whereNotNull('ended_at')
														->groupBy('brcode')
														->orderBy('ended_at','DESC')->get();
		
	
		$currentDate = date('Y-m-d');
		$explodedCurrentDate = explode(" ",$currentDate);
		$jalaliCurrentDate = DateHelper::gregorianToJalaliString($explodedCurrentDate[0]);
		$jalaliExplodedDate = explode('/',$jalaliCurrentDate);
	
		$period = $jalaliExplodedDate[0];
		
		$filledAnswers = [];
		foreach($checklistLatestProcess as $inspectionProcess){
			$filledAnswers[$inspectionProcess['brcode']] = Answer::where('period',$period)
								->where('CHECKLIST_PROCESS_ID',$inspectionProcess->id)
								->where('FK_BRC',$inspectionProcess['brcode'])->count();
		}
		
		foreach($branches as $branchItem){
			
			if(!isset($filledAnswers[$branchItem['brcode']])){
				$filledAnswers[$branchItem['brcode']] = 0;
			}

		}
		
		return $filledAnswers;
		
	}
	
	
	public static function getBranchesStatisticsByAllStatuses($branches){
	
		
	
		$checklistLatestProcess = ChecklistProcess::selectRaw('id, max(brcode) as brcode, max(started_at) as started_at')
														->whereIn('brcode',$branches)
														->groupBy('brcode')
														->orderBy('ended_at','DESC')->get();
		
	
		$currentDate = date('Y-m-d');
		$explodedCurrentDate = explode(" ",$currentDate);
		$jalaliCurrentDate = DateHelper::gregorianToJalaliString($explodedCurrentDate[0]);
		$jalaliExplodedDate = explode('/',$jalaliCurrentDate);
	
		$period = $jalaliExplodedDate[0];
		
		$filledAnswers = [];
		foreach($checklistLatestProcess as $inspectionProcess){
			$filledAnswers[$inspectionProcess['brcode']] = Answer::where('period',$period)
								->where('CHECKLIST_PROCESS_ID',$inspectionProcess->id)
								->where('FK_BRC',$inspectionProcess['brcode'])->count();
		}
		
		
		foreach($branches as $branchItem){
		
			if(!isset($filledAnswers[$branchItem])){
				$filledAnswers[$branchItem] = 0;
			}

		}
		
		return $filledAnswers;
		
	}
	
	
	public static function getInspectedBranchesStatisticsOrdered($branches){
	
		
		$checklistLatestProcess = ChecklistProcess::whereIn('brcode',$branches)
														->whereNotNull('ended_at')
														->orderBy('ended_at','DESC')->get();
	
	
		$currentDate = date('Y-m-d');
		$explodedCurrentDate = explode(" ",$currentDate);
		$jalaliCurrentDate = DateHelper::gregorianToJalaliString($explodedCurrentDate[0]);
		$jalaliExplodedDate = explode('/',$jalaliCurrentDate);
	
		$period = $jalaliExplodedDate[0];
		
		$filledAnswers = [];
		foreach($checklistLatestProcess as $inspectionProcess){
			
			$filledAnswers[$inspectionProcess['brcode']]['answers_count'] = Answer::where('period',$period)
								->where('CHECKLIST_PROCESS_ID',$inspectionProcess->id)
								->where('FK_BRC',$inspectionProcess['brcode'])->count();
								
								
			$explodedDate = explode(" ",$inspectionProcess->started_at);
			$jalaliStartDate = '-';
			if(!empty($explodedDate[0])){
				$jalaliStartDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
			}
			$filledAnswers[$inspectionProcess['brcode']]['started_at_jalali'] = $jalaliStartDate;
			
			
			
			$explodedDate = explode(" ",$inspectionProcess->ended_at);
			$jalaliEndDate = '-';
			if(!empty($explodedDate[0])){
				$jalaliEndDate = DateHelper::gregorianToJalaliString($explodedDate[0]);
			}
			$filledAnswers[$inspectionProcess['brcode']]['ended_at_jalali'] = $jalaliEndDate;

			

		}
		
		return $filledAnswers;
		
	}
	
	
	
	
	public static function getSarparastiBranches($sarcode){
	
		$branches = Branch::select('brcode', 'brname', 'arzi', 'has_rental_box', 'sarcode')
								->where('sarcode',$sarcode);
		
		if(auth()->user()->user_type=='BRANCH_BOSS'){
			$branches = $branches->where('brcode',auth()->user()->brcode);
		}
		
		return $branches->get()->toArray();
		
	}

}

